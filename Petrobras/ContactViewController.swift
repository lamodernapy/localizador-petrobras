//
//  ContactViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/11/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import EZAlertController

class ContactViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    let textFields = [
        ["text": "Nombre y Apellido"],
        ["text": "Celular"],
        ["text": "Mensaje"]
    ]
    
    let actions = [
        ["icon": "icon-phone",
         "text": "021 618 1000"],
        ["icon": "icon-mail",
         "text": "sac.paraguay@petrobras.com"],
        ["icon": "icon-web",
         "text": "www.petrobras.com/paraguay"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Contacto"
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight * 0.15)
    }
    
    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textFields.count + actions.count + 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch  indexPath.row {
        case 0, 1:
            return 100
        case 2:
            return 300
        case 3:
            return 60
        case 4:
            return 80
        case 5, 6, 7:
            return 100
        default:
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ContactTableViewCell = ContactTableViewCell()
        if indexPath.row == 0 || indexPath.row == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "textfield-cell") as! ContactTableViewCell
            let textFieldData = textFields[indexPath.row]
            cell.textField.delegate = self
            cell.textField.tag = indexPath.row
            cell.configureTextFieldCellWith(data: textFieldData)
            
            if indexPath.row == 1 {
                cell.textField.keyboardType = .numberPad
            }
        } else if indexPath.row == 2 {
            cell = tableView.dequeueReusableCell(withIdentifier: "textview-cell") as! ContactTableViewCell
            let textFieldData = textFields[indexPath.row]
            cell.configureTextFieldCellWith(data: textFieldData)
        } else if indexPath.row == 3 {
            cell = tableView.dequeueReusableCell(withIdentifier: "button-cell") as! ContactTableViewCell
        } else if indexPath.row == 4 {
            cell = tableView.dequeueReusableCell(withIdentifier: "title-cell") as! ContactTableViewCell
        } else if indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7 {
            cell = tableView.dequeueReusableCell(withIdentifier: "action-cell") as! ContactTableViewCell
            let actionData = actions[indexPath.row - 5]
            cell.configureActionCellWith(data: actionData)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 5:
            let call = UIAlertAction(title: "Llamar al 021 618 1000", style: .default, handler: { (UIAlertAction) -> Void in
                if let url = URL(string: "tel://0216181000") {
                    UIApplication.shared.openURL(url)
                }
            })
            
            let cancel = UIAlertAction(title: "Cancelar", style: .destructive, handler: { (UIAlertAction) -> Void in })
            EZAlertController.actionSheet("Confirmar llamada", message: "Desea comunicarse con nosotros?", sourceView: self.view, actions: [call, cancel])
        case 6:
            let mail = UIAlertAction(title: "Enviar mail", style: .default, handler: { (UIAlertAction) -> Void in
                if let url = URL(string: "mailto://sac.paraguay@petrobras.com") {
                    UIApplication.shared.openURL(url)
                }
            })
            
            let cancel = UIAlertAction(title: "Cancelar", style: .destructive, handler: { (UIAlertAction) -> Void in })
            EZAlertController.actionSheet("Confirmar envio de mail", message: "Desea comunicarse con nosotros?", sourceView: self.view, actions: [mail, cancel])
        case 7:
            let url = URL(string: "http://www.petrobras.com/paraguay")!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        default:
            break
        }
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        
        Loader.start()
        
        guard
            let nameCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ContactTableViewCell,
            let phoneCell = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? ContactTableViewCell,
            let messageCell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? ContactTableViewCell,
            let buttonCell = tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as? ContactTableViewCell
            else {
                EZAlertController.alert("Error", message: "Favor reintentar.")
                Loader.stop()
                return
        }
        
        buttonCell.button.isEnabled = false
        
        guard
            nameCell.textField.text?.isEmpty == false,
            phoneCell.textField.text?.isEmpty == false,
            messageCell.textView.text?.isEmpty == false
            else {
                EZAlertController.alert("Error", message: "Favor complete todos los campos.")
                buttonCell.button.isEnabled = true
                Loader.stop()
                return
        }
        
        guard
            let nameStr = nameCell.textField.text,
            let phoneStr = phoneCell.textField.text,
            let messageStr = messageCell.textView.text
            else {
                EZAlertController.alert("Error", message: "Favor complete todos los campos.")
                buttonCell.button.isEnabled = true
                Loader.stop()
                return
        }
        
        let params = ["name": nameStr,
                      "phone": phoneStr,
                      "message": messageStr]
        
        ContactRequest.sendMessage(params: params) { success in
            buttonCell.button.isEnabled = true
            Loader.stop()
            switch success {
            case true:
                nameCell.textField.text = ""
                phoneCell.textField.text = ""
                messageCell.textView.text = ""
                EZAlertController.alert("Aviso", message: "Mensaje enviado correctamente.")
            case false:
                EZAlertController.alert("Error", message: "Favor reintentar.")
            }
        }
        
    }
    
    // MARK: Navigation
    
    @IBAction func pushBack(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: AnyObject) {
        pushTo.homeFrom(viewController: self)
    }
}
