//
//  StationListViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/13/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import MapKit
import EZAlertController

class StationListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    var stations: [Station]!
    var services: [Service]?
    var products: [Product]?
    
    override func viewWillAppear(_ animated: Bool) {
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.serviceData, completion: { (result, object) in
            if result == true {
                self.services = object as? [Service]
            }
        })
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.productsData, completion: {(result, object) in
            if result == true {
                self.products = object as? [Product]
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		setNavbarDivider()

        self.title = "Búsqueda"
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight * 0.15)
        tableView.register(UINib(nibName: "StationCell", bundle: nil), forCellReuseIdentifier: "station-cell")
        tableView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
    }

    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stations.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "station-cell", for: indexPath) as! StationTableViewCell
        cell.backgroundColor = .clear
        let station = stations[indexPath.row]
        var products: [Product] = []
        for productHelper in station.products! {
            let product = self.products?.first(where: { productHelper == $0.id })
            products.append(product!)
        }
        cell.products = products
        cell.station = station
        if let servicesHelper = self.services {
            let services = servicesHelper.sorted(by: { $0.priority! > $1.priority! })
            cell.services = services
        }
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let station = stations[indexPath.row]
        openNavigation(forStation: station)
    }
    
    func openNavigation (forStation: Station) {
        let maps = UIAlertAction(title: "Maps", style: .default, handler: { (UIAlertAction) -> Void in
            let coordinate = CLLocationCoordinate2DMake(forStation.latitude, forStation.longitude)
            let region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.01, 0.02))
            let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: region.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: region.span)]
            mapItem.name = forStation.name
            mapItem.openInMaps(launchOptions: options)
        })
        let gMaps = UIAlertAction(title: "Google maps", style: .default, handler: { (UIAlertAction) -> Void in
            
            let latitude = String(forStation.latitude)
            let longitude = String(forStation.longitude)
            
            UIApplication.shared.openURL(URL(string:
                "comgooglemaps://??saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!)
            
        })
        let cancel = UIAlertAction(title: "Cancelar", style: .destructive, handler: { (UIAlertAction) -> Void in
            print("Second Button pressed")
        })
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            EZAlertController.actionSheet("Alerta!", message: "Cuál aplicación desea abrir?", sourceView: self.view, actions: [maps, gMaps, cancel])
            
        } else {
            EZAlertController.actionSheet("Alerta!", message: "Desea abrir?", sourceView: self.view, actions: [maps, cancel])
        }
    }
    
    // MARK: Navigation
    
    @IBAction func pushBack(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }

    @IBAction func goHome(_ sender: AnyObject) {
        performSegue(withIdentifier: SegueIdentifier.pusthToGMaps, sender: nil)
    }
    
}
