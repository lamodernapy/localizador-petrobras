//
//  HeaderStationTableViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/12/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class HeaderStationTableViewCell: UITableViewCell {

    @IBOutlet var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
