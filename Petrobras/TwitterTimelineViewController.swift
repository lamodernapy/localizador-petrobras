//
//  TwitterTimelineViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 7/11/18.
//  Copyright © 2018 Agencia Lamoderna. All rights reserved.
//

import UIKit
import TwitterKit

class TwitterTimelineViewController: TWTRTimelineViewController {

    convenience init() {
        let dataSource = TWTRUserTimelineDataSource(screenName: "fabric", apiClient: TWTRAPIClient())
        self.init(dataSource: dataSource)
        self.showTweetActions = true
    }
    
    override required init(dataSource: TWTRTimelineDataSource?) {
        super.init(dataSource: dataSource)
        self.showTweetActions = true
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
