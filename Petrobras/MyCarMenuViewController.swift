//
//  MyCarMenuViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/2/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class MyCarMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    let menu = [
        ["icon": "icon-menu-fuel",
         "text": "Cargar combustible"],
        ["icon": "icon-menu-maintenance",
         "text": "Costo de mantenimiento"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: (ScreenSize.height * 0.5) - 120 - 64)
        tableView.tableFooterView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: (ScreenSize.height * 0.5) - 120)
    }
    
    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menu-cell") as! MyCarMenuTableViewCell
        let data = menu[indexPath.row]
        cell.configureWith(data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.performSegue(withIdentifier: "showFuelDataForm", sender: nil)
        case 1:
            self.performSegue(withIdentifier: "showMaintenanceForm", sender: nil)
        default:
            break
        }
    }
    
    // MARK: Navigation
    
    @IBAction func pushBack(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
}
