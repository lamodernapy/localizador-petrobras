//
//  TwitterTableViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 7/16/18.
//  Copyright © 2018 Agencia Lamoderna. All rights reserved.
//

import UIKit

class TwitterTableViewCell: UITableViewCell {

    @IBOutlet weak var wrapperView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        wrapperView.layer.borderWidth = 2
        wrapperView.layer.borderColor = UIColor(red:0.84, green:0.84, blue:0.84, alpha:1.0).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
