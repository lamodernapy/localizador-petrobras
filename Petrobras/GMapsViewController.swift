//
//  GMapsViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/20/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import GoogleMaps
import EZAlertController

class GMapsViewController: UIViewController {
    
    @IBOutlet var stationsButton: UIButton!
    @IBOutlet var shopsButton: UIButton!
    @IBOutlet var headerBg: UIView!
    @IBOutlet var wrapperButtons: UIView!

    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var zoomLevel: Float = 13.0
    var nearestStations: [Station]?
    var flagStations: Bool = true
    
    @IBAction func showStations(_ sender: Any) {
        flagStations = true
        updateMarkers()
        stationsButton.isSelected = true
        shopsButton.isSelected = false
    }
    
    @IBAction func showShops(_ sender: Any) {
        flagStations = false
        updateMarkers()
        stationsButton.isSelected = false
        shopsButton.isSelected = true
    }
    
    // A default location to use when location permission is not granted.
    let defaultLocation = CLLocation(latitude: -33.8523341, longitude: 151.2106085)
    
    override func viewWillAppear(_ animated: Bool) {
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.nearestStationData, completion: {(result, object) in
            if result == true {
                self.nearestStations = object as? [Station]
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Búsqueda"
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        stationsButton.setTitleColor(CustomColors.yellow, for: .selected)
        shopsButton.setTitleColor(CustomColors.yellow, for: .selected)
        
        stationsButton.isSelected = true
        
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        // Create a map.
        let camera = GMSCameraPosition.camera(withLatitude: defaultLocation.coordinate.latitude,
                                              longitude: defaultLocation.coordinate.longitude,
                                              zoom: zoomLevel)
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        
        
        // Add the map to the view, hide it until we've got a location update.
        view.addSubview(mapView)
        mapView.isHidden = true
        
        let position = CLLocationCoordinate2D(latitude: defaultLocation.coordinate.latitude, longitude: defaultLocation.coordinate.longitude)
        let marker = GMSMarker(position: position)
        marker.map = mapView
        
        view.bringSubview(toFront: wrapperButtons)
        view.bringSubview(toFront: headerBg)
    }
    
    // Add markers for the places nearby the device.
    func updateMarkers() {
        mapView.clear()
        guard let stations = self.nearestStations else {
            EZAlertController.alert("Atención", message: "No tenemos los datos de las estaciones.")
            Loader.stop()
            return
        }
        
        var tenNearest: ArraySlice<Station> = []
        
        if flagStations == true {
            tenNearest = stations[0..<15]
        } else {
            var x: Int = 0
            for (_, station) in stations.enumerated() {
                if x < 15 {
                    let sape = station.services?.contains(where: { $0 == "c9f0f895fb" })
                    if sape == true {
                        tenNearest.append(station)
                        x += 1
                    }
                }
            }
        }
        
        for (_, station) in tenNearest.enumerated() {
            let position = CLLocationCoordinate2D(latitude: station.latitude, longitude: station.longitude)
            let marker = GMSMarker(position: position)
            marker.title = station.name
            marker.snippet = station.address
            marker.icon = flagStations == true ? UIImage(named: "icon-station") : UIImage(named: "icon-spacio")
            marker.map = mapView
            marker.appearAnimation = GMSMarkerAnimation.pop
        }
    }
    
    // MARK: Navigation
    
    @IBAction func pushBack(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func goHome(_ sender: AnyObject) {
        performSegue(withIdentifier: SegueIdentifier.pusthToGMaps, sender: nil)
    }
}

// Delegates to handle events for the location manager.
extension GMapsViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
        guard let stations = self.nearestStations else {
            EZAlertController.alert("Atención", message: "No tenemos los datos de las estaciones.")
            Loader.stop()
            return
        }
        
        for station in stations {
            station.calculateDistance(fromLocation: location)
        }
        
        nearestStations = stations.sorted(by: {
            $0.distance < $1.distance
        })
        
        UserDefaultsClasses.save(object: nearestStations!, withKey: UserDefaultsKey.nearestStationData)
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
        updateMarkers()
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .notDetermined:
            EZAlertController.alert("Atención", message: "No tenemos autorización para acceder a tu ubicación.")
        case .denied:
            EZAlertController.alert("Atención", message: "No tenemos autorización para acceder a tu ubicación.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
    
}
