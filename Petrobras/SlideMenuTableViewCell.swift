//
//  SlideMenuTableViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 11/28/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class SlideMenuTableViewCell: UITableViewCell {

    @IBOutlet var icon: UIImageView!
    @IBOutlet var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
