//
//  FuelDataViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/1/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class FuelDataViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    let fields = [
        ["icon": "",
         "text": "Kilometraje actual:"],
        ["icon": "",
         "text": "Litros cargados:"],
        ["icon": "",
         "text": "Monto cargado:"],
        ["icon": "",
         "text": "0"]
    ]
    
    var km: Int = 0
    var lts: Int = 0
    var amount: Int = 0
    var cellSelected: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Cargar combustible"
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height * 0.15)
        tableView.tableFooterView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height * 0.2)
    }
    
    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fields.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MyCarTableViewCell!
        let field = fields[indexPath.row]
        switch indexPath.row {
        case 3:
            cell = tableView.dequeueReusableCell(withIdentifier: "label-cell") as! MyCarTableViewCell
            cell.configureWith(data: field)
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "textview-cell") as! MyCarTableViewCell
            cell.textfield.delegate = self
            cell.textfield.tag = indexPath.row
            cell.configureWith(data: field)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cellSelected = indexPath.row
        let cell = tableView.cellForRow(at: indexPath) as! MyCarTableViewCell
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 {
            cell.textfield.becomeFirstResponder()
        }
    }
    
    @IBAction func saveMyFuelData(_ sender: AnyObject) {
        if km != 0 && lts != 0 && amount != 0 {
            var fuelDataArray: [FuelData] = []
            let data = UserDefaults.standard.object(forKey: UserDefaultsKey.fuelData) as? Data
            if data == nil {
                let fuelDic = ["km": km,
                               "lts": lts,
                               "amount": amount,
                               "consumption": 0]
                let fuelData = FuelData(dictionary: fuelDic)
                fuelDataArray.append(fuelData)
                let saveFuelData  = NSKeyedArchiver.archivedData(withRootObject: fuelDataArray)
                print(saveFuelData)
                UserDefaults.standard.set(saveFuelData, forKey: UserDefaultsKey.fuelData)
                let allViewController = self.navigationController?.viewControllers
                for viewController in allViewController! {
                    if viewController.isKind(of: MyCarProfileViewController.self) {
                        _ = viewController.navigationController?.popToViewController(viewController, animated: true)
                    }
                }
            } else {
                fuelDataArray = NSKeyedUnarchiver.unarchiveObject(with: data!) as! [FuelData]
                if let lastKm = fuelDataArray.last?.km {
                    if lastKm < km {
                        let consumption = (km - lastKm) / lts
                        let fuelDic = ["km": km,
                                       "lts": lts,
                                       "amount": amount,
                                       "consumption": consumption]
                        let fuelData = FuelData(dictionary: fuelDic)
                        fuelDataArray.append(fuelData)
                        let saveFuelData  = NSKeyedArchiver.archivedData(withRootObject: fuelDataArray)
                        print(saveFuelData)
                        UserDefaults.standard.set(saveFuelData, forKey: UserDefaultsKey.fuelData)
                        let allViewController = self.navigationController?.viewControllers
                        for viewController in allViewController! {
                            if viewController.isKind(of: MyCarProfileViewController.self) {
                                _ = viewController.navigationController?.popToViewController(viewController, animated: true)
                            }
                        }
                    } else {
                        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! MyCarTableViewCell
                        cell.message.text = "El kilometraje debe ser mayor a la anterior carga."
                    }
                }
            }
        } else {
            for i in 0..<3 {
                let cell = tableView.cellForRow(at: IndexPath(row: i, section: 0)) as! MyCarTableViewCell
                switch i {
                case 0:
                    cell.message.text = km == 0 ? "Ingrese su kilometraje actual." : ""
                case 1:
                    cell.message.text = lts == 0 ? "Ingrese los litros cargados." : ""
                case 2:
                    cell.message.text = amount == 0 ? "Ingrese el monto de su carga." : ""
                default:
                    break
                }   
            }
        }
    }
    
    // MARK: TextField
    
    @IBAction func textFieldEdit(_ sender: UITextField) {
        if let text = sender.text {
            cellSelected = sender.tag
            switch cellSelected {
            case 0:
                km = text.isEmpty == false ? Int(text)! : 0
            case 1:
                lts = text.isEmpty == false ? Int(text)! : 0
            case 2:
                amount = text.isEmpty == false ? Int(text)! : 0
            default:
                break
            }
            let cell = tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! MyCarTableViewCell
            cell.titleOption.text = lts != 0 && amount != 0 ? String(amount / lts)  : "0"
        }
    }

    // MARK: - Navigation

    @IBAction func pushBack(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: AnyObject) {
        pushTo.homeFrom(viewController: self)
    }
    
}
