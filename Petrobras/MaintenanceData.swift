//
//  MaintenanceData.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/9/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation

class MaintenanceData: NSObject, NSCoding {
    var km: Int!
    var amount: Int!
    var maintenance: Maintenance!
    var date: Date!
    
    init (dictionary: [String: Any]) {
        km = dictionary["km"] as? Int
        amount = dictionary["amount"] as? Int
        maintenance = dictionary["maintenance"] as? Maintenance
        date = dictionary["date"] as? Date
    }
    
    required init(coder aDecoder: NSCoder) {
        km = aDecoder.decodeObject(forKey: "km") as? Int
        amount = aDecoder.decodeObject(forKey: "amount") as? Int
        maintenance = aDecoder.decodeObject(forKey: "maintenance") as? Maintenance
        date = aDecoder.decodeObject(forKey: "date") as? Date
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(km, forKey: "km")
        aCoder.encode(amount, forKey: "amount")
        aCoder.encode(maintenance, forKey: "maintenance")
        aCoder.encode(date, forKey: "date")
    }
}
