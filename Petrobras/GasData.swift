//
//  GasData.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/15/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation

class GasData: NSObject, NSCoding {
    var date: Date!
    
    init (date: Date) {
        self.date = date
    }
    
    required init(coder aDecoder: NSCoder) {
        date = aDecoder.decodeObject(forKey: "date") as? Date
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(date, forKey: "date")
    }
}

