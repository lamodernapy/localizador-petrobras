//
//  EventDetailViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 7/11/18.
//  Copyright © 2018 Agencia Lamoderna. All rights reserved.
//

import UIKit
import TwitterKit
import Alamofire

class EventDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var event: Event!
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		setNavbarDivider()
		
        self.title = "Detalle del Evento"
        
        checkKml()
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight * 0.15)
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
        tableView.register(UINib(nibName: "MapMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MapMenuTableViewCell")
        tableView.register(UINib(nibName: "TwitterTableViewCell", bundle: nil), forCellReuseIdentifier: "TwitterTableViewCell")
        tableView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
		
		tableView.estimatedRowHeight = 60
		tableView.rowHeight = UITableViewAutomaticDimension
		
    }
    
    func checkKml() {
        let split = event.kmz.url?.components(separatedBy: "/")
        let file = split![1].components(separatedBy: ".")
        let fileName = file[0]
        let fileExtension = file[1]
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent("\(fileName).\(fileExtension)")
        let fileExists = FileManager().fileExists(atPath: fileURL.path)
        
        if fileExists == false {
            UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.settingData, completion: { (result, object) in
                switch result {
                case true:
                    let cdnUrl = object as? String
                    let fileUrl = "\(cdnUrl!)\(String(describing: self.event.kmz.url!))"
            
                    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                        var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                        documentsURL.appendPathComponent("\(fileName).\(fileExtension)")
                        return (documentsURL, [.removePreviousFile])
                    }
                    
                    Alamofire.download(fileUrl, to: destination).response { response in
                        if response.destinationURL != nil {
                            print(response.destinationURL!)
                        }
                    }
                case false:
                    break
                }
            })
        }
        
        
        
    }
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            let identifier = segue.identifier,
            let vc = segue.destination as? RallyMapViewController
            else {
                return
        }
        if identifier == SegueIdentifier.pushToRallyMap {
            vc.event = event
        }
        
    }
    
    @IBAction func pushBack(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: Any) {
        pushTo.homeFrom(viewController: self)
    }

}

extension EventDetailViewController: UITableViewDataSource {
	
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
            cell.backgroundColor = .clear
            cell.date.text = "\(event.date!)"
            cell.title.text = event.title
            cell.content.text = event.desc
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MapMenuTableViewCell", for: indexPath) as! MapMenuTableViewCell
            cell.backgroundColor = .clear
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TwitterTableViewCell", for: indexPath) as! TwitterTableViewCell
            cell.backgroundColor = .clear
            return cell
        default:
            return UITableViewCell()
        }
    }
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		switch indexPath.row {
		case 0:
			return UITableViewAutomaticDimension
		case 1:
			return 120
		case 2:
			return 100
		default:
			return 0
		}
	}
}

extension EventDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            self.performSegue(withIdentifier: SegueIdentifier.pushToRallyMap, sender: nil)
        case 2:
            guard let hashtags = event.hashtags else {
                return
            }
            print(hashtags[0])
            let dataSource = TWTRSearchTimelineDataSource(searchQuery: hashtags[0], apiClient: TWTRAPIClient())
            let vc = TwitterTimelineViewController(dataSource: dataSource)
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
}
