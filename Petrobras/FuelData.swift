//
//  FuelData.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/9/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation

class FuelData: NSObject, NSCoding {
    var km: Int!
    var lts: Int!
    var amount: Int!
    var consumption: Int!
    var date: Date!
    
    
    init (dictionary: [String: Any]) {
        km = dictionary["km"] as? Int
        lts  = dictionary["lts"] as? Int
        amount = dictionary["amount"] as? Int
        consumption = dictionary["consumption"] as? Int
        date = Date()
    }
    
    required init(coder aDecoder: NSCoder) {
        km = aDecoder.decodeObject(forKey: "km") as? Int
        lts  = aDecoder.decodeObject(forKey: "lts") as? Int
        amount = aDecoder.decodeObject(forKey: "amount") as? Int
        consumption = aDecoder.decodeObject(forKey: "consumption") as? Int
        date = aDecoder.decodeObject(forKey: "date") as? Date
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(km, forKey: "km")
        aCoder.encode(lts, forKey: "lts")
        aCoder.encode(amount, forKey: "amount")
        aCoder.encode(consumption, forKey: "consumption")
        aCoder.encode(date, forKey: "date")
    }
}
