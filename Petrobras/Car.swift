//
//  Car.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/3/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation

class Car: NSObject, NSCoding {
    var brand: Brand?
    var year: String?
    var model: Model?
    
    init (dictionary: [String: Any]) {
        brand = dictionary["brand"] as? Brand
        year  = dictionary["year"] as? String
        model = dictionary["model"] as? Model
    }
    
    required init(coder aDecoder: NSCoder) {
        brand = aDecoder.decodeObject(forKey: "brand") as? Brand
        year  = aDecoder.decodeObject(forKey: "year") as? String
        model = aDecoder.decodeObject(forKey: "model") as? Model
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(brand, forKey: "brand")
        aCoder.encode(year, forKey: "year")
        aCoder.encode(model, forKey: "model")
    }
}
