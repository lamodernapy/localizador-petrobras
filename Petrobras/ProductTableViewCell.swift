//
//  ProductTableViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 11/28/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet var icon: UIImageView!
    @IBOutlet var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        switch selected {
        case true:
            print("true")
            title.textColor = .white
            icon.image = UIImage(named: "icon-checkon-white")
        case false:
            print("false")
            title.textColor = CustomColors.textColor
            icon.image = UIImage(named: "icon-checkon-blue")
        }
    }

//    func setSelected(_ selected: Bool, animated: Bool, data: [String: String]) {
//        super.setSelected(selected, animated: animated)
//        
//    }
}
