//
//  LauncherViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/5/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import Alamofire

class LauncherViewController: UIViewController {
    
    var notification = false
    var promoId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Loader.start()
		
		// Disable cache
		URLCache.shared = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
    
        Alamofire.request(RequestUrl.masterJson)
            .responseJSON { response in
                switch response.result {
                case .success:
                    
					print("MasterJSON Result Value: \(String(describing: response.result.value))")
                    
                    guard let responseJSON = response.result.value as? [String: AnyObject] else {
                        print("Can't parse MasterJSON value")
                        Loader.stop()
                        self.performSegue(withIdentifier: SegueIdentifier.pushToHome, sender: nil)
                        return
                    }
					
					print("Response JSON: \(responseJSON)")
                    
                    
                    self.saveBrands(dataJSON: responseJSON)
                    
                    self.saveStation(dataJSON: responseJSON)
                    
                    self.saveProduct(dataJSON: responseJSON)
                    
                    self.saveMaintenance(dataJSON: responseJSON)
                    
                    self.saveFiles(dataJSON: responseJSON)
                    
                    self.saveServices(dataJSON: responseJSON)
                    
                    self.saveRegions(dataJSON: responseJSON)
                    
                    self.savePromo(dataJSON: responseJSON)
                    
                    self.saveSettings(dataJSON: responseJSON)
                    
                    self.saveEvents(dataJSON: responseJSON)
                    
                    if self.notification == true {
                        guard let promoId = self.promoId else {
                            print("no promo id")
                            return
                        }
                        
                        Alamofire.request("\(RequestUrl.root)\(RequestUrl.promo)/\(promoId)?expand=image")
                            .responseJSON{ response in
                                switch response.result {
                                case .success:
                                    guard let responseJSON = response.result.value as? [String: Any] else {
                                        print("no hay data")
                                        return
                                    }
                                    
                                    guard let success = responseJSON["success"] as? Bool else {
                                        print("no hay data")
                                        return
                                    }
                                    
                                    switch success {
                                    case true:
                                        guard let data = responseJSON["result"] as? [String: Any] else {
                                            print("no hay data")
                                            return
                                        }
                                        
                                        let promo = Promo.init(notificationDictionary: data)
                                        
                                        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                                        let vc = storyboard.instantiateViewController(withIdentifier: "promo-detail") as! PromoDetailViewController
                                        vc.promo = promo
                                        vc.notification = true
                                        
                                        let nvc: UINavigationController = UINavigationController(rootViewController: vc)
                                        nvc.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                                        nvc.navigationBar.shadowImage = UIImage()
                                        nvc.navigationBar.isTranslucent = true
                                        nvc.navigationBar.titleTextAttributes = [
                                            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
                                        ]
                                        
                                        self.present(nvc, animated: true, completion: nil)
                                    case false:
                                        print("no hay data")
                                        return
                                    }
                                case .failure( _):
                                    print("no hay data")
                                    return
                                }
                                
                                
                        }
                    }
                    
                    self.perform(#selector(self.push), with: self, afterDelay: 3)
                case .failure( _):
                    Loader.stop()
                    self.performSegue(withIdentifier: SegueIdentifier.pushToHome, sender: nil)
                }
                
        }
    }
    
    @objc func push() {
        Loader.stop()
        self.performSegue(withIdentifier: SegueIdentifier.pushToHome, sender: nil)
    }
    
    // MARK: - Brand & Models
    
    func saveBrands(dataJSON: [String: AnyObject]) {
        guard let brandsHelper = dataJSON["brands"] as? [[String: AnyObject]] else {
            print("no brand")
            return
        }
        
        var brands: [Brand] = []
        
        for (_, brandsDic) in brandsHelper.enumerated() {
            let brand = Brand(dictionary: brandsDic)
            brands.append(brand)
        }
        
        UserDefaultsClasses.save(object: brands, withKey: UserDefaultsKey.brandsData)
    }
    
    // MARK: - Station
    
    func saveStation(dataJSON: [String: AnyObject]) {
        guard let stationsHelper = dataJSON["stations"] as? [[String: AnyObject]] else {
            print("no station")
            return
        }
        
        var stations: [Station] = []
        
        for (_, stationDic) in stationsHelper.enumerated() {
            let station = Station(dictionary: stationDic)
            stations.append(station)
        }
        
        UserDefaultsClasses.save(object: stations, withKey: UserDefaultsKey.stationsData)
    }

    // MARK: - Product
    
    func saveProduct(dataJSON: [String: AnyObject]) {
        guard let productsHelper = dataJSON["fuels"] as? [[String: AnyObject]] else {
            print("no products")
            return
        }
        
        var products: [Product] = []
        
        for (_, productDic) in productsHelper.enumerated() {
            let product = Product(dictionary: productDic)
            products.append(product)
        }
        
        UserDefaultsClasses.save(object: products, withKey: UserDefaultsKey.productsData)
    }
    
    // MARK: - Maintenance
    
    func saveMaintenance(dataJSON: [String: AnyObject]) {
        guard let maintenanceHelper = dataJSON["maintenances"] as? [[String: AnyObject]] else {
            print("no maintenance")
            return
        }
        
        var maintenances: [Maintenance] = []
        
        for (_, maintenanceDic) in maintenanceHelper.enumerated() {
            let maintenance = Maintenance(dictionary: maintenanceDic)
            maintenances.append(maintenance)
        }
        
        UserDefaultsClasses.save(object: maintenances, withKey: UserDefaultsKey.maintenanceListData)
    }
    
    // MARK: - Services
    
    func saveServices(dataJSON: [String: AnyObject]) {
        guard let serviceHelper = dataJSON["services"] as? [[String: AnyObject]] else {
            print("no service")
            return
        }
        
        var services: [Service] = []
        
        for (_, serviceDic) in serviceHelper.enumerated() {
            let service = Service(dictionary: serviceDic)
            services.append(service)
        }
        
        UserDefaultsClasses.save(object: services, withKey: UserDefaultsKey.serviceData)
    }
    
    // MARK: - Files
    
    func saveFiles(dataJSON: [String: AnyObject]) {
        guard let fileHelper = dataJSON["files"] as? [[String: AnyObject]] else {
            print("no files")
            return
        }
        
        var files: [File] = []
        
        for (_, fileDic) in fileHelper.enumerated() {
            let file = File(dictionary: fileDic)
            files.append(file)
        }
        
        UserDefaultsClasses.save(object: files, withKey: UserDefaultsKey.filesData)
    }
    
    // MARK: - Regions
    
    func saveRegions(dataJSON: [String: AnyObject]) {
        guard let regionsHelper = dataJSON["regions"] as? [[String: AnyObject]] else {
            print("no regions")
            return
        }
        
        var regions: [Region] = []
        
        for (_, regionDic) in regionsHelper.enumerated() {
            let region = Region(dictionary: regionDic)
            regions.append(region)
        }
        
        UserDefaultsClasses.save(object: regions, withKey: UserDefaultsKey.regionData)
    }
    
    // MARK: - Promo
    
    func savePromo(dataJSON: [String: AnyObject]) {
        guard let promosHelper = dataJSON["promotions"] as? [[String: AnyObject]] else {
            print("no promo")
            return
        }
        
        var promos: [Promo] = []
        
        for (_, promoDic) in promosHelper.enumerated() {
            let promo = Promo(dictionary: promoDic)
            promos.append(promo)
        }
        
        UserDefaultsClasses.save(object: promos, withKey: UserDefaultsKey.promoData)
    }
    
    // MARK: - Settings
    
    func saveSettings(dataJSON: [String: AnyObject]) {
        guard
            let settingHelper = dataJSON["settings"] as? [String: String],
            let cdn = settingHelper["cdn"]
            else {
            print("no settings")
            return
        }
        
        UserDefaultsClasses.save(object: cdn, withKey: UserDefaultsKey.settingData)
    }
    
    func saveEvents(dataJSON: [String: AnyObject]) {
        guard let eventsHelper = dataJSON["events"] as? [[String: AnyObject]] else {
            print("no events")
            return
        }
        
        var events: [Event] = []
        
        for (_, eventsDic) in eventsHelper.enumerated() {
            let event = Event(dictionary: eventsDic)
            events.append(event)
        }
        
        print(events)
        
        UserDefaultsClasses.save(object: events, withKey: UserDefaultsKey.eventsData)
    }
}
