//
//  User.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 1/13/17.
//  Copyright © 2017 Agencia Lamoderna. All rights reserved.
//

import Foundation
import Alamofire
import CryptoSwift

class User: NSObject, NSCoding {
    var imei: String?
    var id: String?
    var name: String?
    var type: String?
    var password: String?
    
    init (dictionary: [String: Any], phoneId: String) {
        imei = phoneId
        id = dictionary["user"] as? String
        name = dictionary["name"] as? String
        type = dictionary["type"] as? String
        
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date) < 10 ? "0\(calendar.component(.year, from: date))" : "\(calendar.component(.year, from: date))"
        let month = calendar.component(.month, from: date) < 10 ? "0\(calendar.component(.month, from: date))" : "\(calendar.component(.month, from: date))"
        let day = calendar.component(.day, from: date) < 10 ? "0\(calendar.component(.day, from: date))" : "\(calendar.component(.day, from: date))"
        
        let passwordStr = "\(year)\(phoneId)\(month)\(day)"
        
        let utf8str = passwordStr.data(using: String.Encoding.utf8)
        let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        password = (base64Encoded?.md5())!
    }
    
    required init(coder aDecoder: NSCoder) {
        imei = aDecoder.decodeObject(forKey: "imei") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        name  = aDecoder.decodeObject(forKey: "name") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        password = aDecoder.decodeObject(forKey: "password") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(imei, forKey: "imei")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(type, forKey: "type")
        aCoder.encode(password, forKey: "password")
    }
}

class UserRequest {
    static func register(phoneId: String, completion:@escaping (_ result: Bool) -> Void) {
        let url = ("\(RequestUrl.root)\(RequestUrl.register)")
        let params = ["imei": phoneId]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                switch response.result.isSuccess {
                case true:
                    if let responseJSON = response.result.value as? [String: AnyObject],
                        let success = responseJSON["success"] as? Bool {
                        switch success {
                        case true:
                            let user = User.init(dictionary: responseJSON, phoneId: phoneId)
                            print(user)
                            UserDefaultsClasses.save(object: user, withKey: UserDefaultsKey.userData)
                            completion(true)
                        case false:
                            completion(false)
                        }
                    }
                case false:
                    print("Error: \(response.result.error)")
                    completion(false)
                }
        }
    }
    
    class func createSession(_ user: User, completion:@escaping (_ result: Bool) -> Void) {
        let url = ("\(RequestUrl.root)\(RequestUrl.login)")
        let params = ["user": user.imei!, "pass": user.password!]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { (response) in
                print("create session: \(response)")
                if let
                    _ = response.response?.allHeaderFields as? [String: String],
                    let URL = response.request?.url
                {
                    let cookies = SessionManager.default.session.configuration.httpCookieStorage?.cookies(for: URL)
                    SessionManager.default.session.configuration.httpCookieStorage?.setCookies(cookies!, for: URL, mainDocumentURL: nil)
                    guard response.result.isSuccess else {
                        print("Error: \(response.result.error)")
                        completion(false)
                        return
                    }
                    
                    guard let
                        responseJSON = response.result.value as? [String: AnyObject],
                        let success = responseJSON["success"] as? Bool
                        else {
                            print("Invalid Data")
                            completion(false)
                            return
                    }
                    
                    if success == true {
                        UserDefaultsClasses.save(object: cookies!, withKey: UserDefaultsKey.cookies)
                        completion(true)
                    } else {
                        completion(false)
                    }
                }
        }
    }
    
    class func checkSession(_ completion:@escaping (_ result: Bool) -> Void) {
        let url = ("\(RequestUrl.root)\(RequestUrl.login)")
        
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.cookies) { (result, data) in
            switch result {
            case true:
                let cookies = data as! [HTTPCookie]
                SessionManager.default.session.configuration.httpCookieStorage?.setCookies(cookies, for: URL(string: url), mainDocumentURL: nil)
                Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                    .responseJSON { response in
                        print("check session: \(response)")
                        guard response.result.isSuccess else {
                            print("Error: \(response.result.error)")
                            completion(false)
                            return
                        }
                        
                        guard let
                            responseJSON = response.result.value as? [String: AnyObject],
                            let success = responseJSON["success"] as? Bool,
                            let logged = responseJSON["logged"] as? Bool
                            else {
                                print("Invalid Data")
                                completion(false)
                                return
                        }
                        
                        success == true && logged == true ? completion(true) : completion(false)
                }
            case false:
                break
            }
        }
    }
    
    class func saveTokenForNotification(_ token: String, completion:@escaping (_ result: Bool) -> Void) {
        let url = ("\(RequestUrl.root)\(RequestUrl.login)")
        let params = ["ios": token]
        
        Alamofire.request(url, method: .put, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { (response) in
                switch response.result.isSuccess {
                case true:
                    if let responseJSON = response.result.value as? [String: AnyObject],
                        let success = responseJSON["success"] as? Bool {
                        switch success {
                        case true:
                            completion(true)
                        case false:
                            completion(false)
                        }
                    }
                case false:
                    print("Error: \(response.result.error)")
                    completion(false)
                }
        }
    }
}


class ContactRequest {
    static func sendMessage(params: [String: String], completion:@escaping (_ result: Bool) -> Void) {
        let url = ("\(RequestUrl.root)\(RequestUrl.contact)")
        
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                print(response)
                switch response.result.isSuccess {
                case true:
                    if let responseJSON = response.result.value as? [String: AnyObject],
                        let success = responseJSON["success"] as? Bool {
                        switch success {
                        case true:
                            completion(true)
                        case false:
                            completion(false)
                        }
                    }
                case false:
                    print("Error: \(response.result.error)")
                    completion(false)
                }
        }
    }
}
