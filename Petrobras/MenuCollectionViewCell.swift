//
//  MenuCollectionViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 11/24/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var title: UILabel!
    
    
    func setSelected(_ selected: Bool, animated: Bool) {
        super.isSelected = selected
        
//        imageContent.layer.
        
        switch selected {
        case true:
            imageView.layer.cornerRadius = imageView.frame.size.height / 2
            imageView.layer.borderWidth = 4
            imageView.layer.borderColor = UIColor(red:0.00, green:0.70, blue:0.67, alpha:1.0).cgColor
        case false:
            imageView.layer.borderWidth = 0
        }
    }
    
}
