//
//  Fuel.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/6/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation

class Product: NSObject, NSCoding {
    var id: String?
    var name: String?
    var price: String?
    var octanes: Int?
    var priority: Int?
    
    init (dictionary: [String: Any]) {
        id = dictionary["id"] as? String
        name  = dictionary["name"] as? String
        price = dictionary["price"] as? String
        octanes = dictionary["octanes"] as? Int
        priority = dictionary["priority"] as? Int
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? String
        name  = aDecoder.decodeObject(forKey: "name") as? String
        price = aDecoder.decodeObject(forKey: "price") as? String
        octanes = aDecoder.decodeObject(forKey: "octanes") as? Int
        priority = aDecoder.decodeObject(forKey: "priority") as? Int
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(price, forKey: "price")
        aCoder.encode(octanes, forKey: "octanes")
        aCoder.encode(priority, forKey: "priority")
    }
}
