//
//  PickerViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/6/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

protocol PickerDelegate {
    func userSelect(index: Int)
}

class PickerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    
    var options: [String] = []
    var selectedOptions: Int?
    var delegate: PickerDelegate?
    var titleForNavigationBar: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if titleForNavigationBar != nil {
            self.navigationController?.navigationBar.titleTextAttributes = [
                NSAttributedStringKey.foregroundColor: CustomColors.blue,
                NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
            ]
            self.title = titleForNavigationBar
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if selectedOptions != nil {
            tableView.selectRow(at: IndexPath(row: selectedOptions!, section: 0), animated: true, scrollPosition: .none)
        }
    }
    
    // MARK: TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if selectedOptions != nil {
            if indexPath.row == selectedOptions {
                cell.setSelected(true, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.picker) as! PickerTableViewCell
        cell.title.text = options[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            delegate?.userSelect(index: indexPath.row)
            if let nav = self.navigationController {
                nav.popViewController(animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    // MARK: Navigation
    
    @IBAction func pushBack(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    

}
