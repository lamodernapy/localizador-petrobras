# Change Log
Todos los cambios notables para este proyecto deben ser documentados en este archivo.
Este proyecto sigue las convenciones del popular [Versionado Semántico](http://semver.org/) y de [Keep a CHANGELOG](http://keepachangelog.com/).

## [5.3] 2017-03-28
### Added
- Version enviada al cliente para subir al AppStore
