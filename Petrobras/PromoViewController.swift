//
//  PromoViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/16/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import EZAlertController

class PromoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    var promos: [Promo]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		setNavbarDivider()
		preloadPromoData()
		
        self.title = "Novedades y Promociones"
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        tableView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
    }
	
	func preloadPromoData() {
		
		UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.promoData, completion: { (result, object) in
			if result == true {
				let promos = object as? [Promo]
				
				guard let promosHelper = promos else {
					EZAlertController.alert("No tenemos promos para mostrarte", message: "")
					return
				}
				
				let currentDate = Date()
				let myCalendar = Calendar(identifier: .gregorian)
				let weekDay = myCalendar.component(.weekday, from: currentDate) - 1
				
				var helper: [Promo] = []
				
				for (_, promo) in promosHelper.enumerated() {
					guard
						let start = promo.startDate,
						let end = promo.finishDate
					else {
						return
					}
					
					if (start...end).contains(Date()) == true {
						let binaryString = String(promo.days, radix: 2)
						let binaryPad = self.pad(string: binaryString, toSize: 7)
						
						let reverseBinaryPad = binaryPad.reversed()
						
						for (i, value) in reverseBinaryPad.enumerated() {
							let character = String(value)
							
							if i == weekDay && character == "1" {
								print("agrega")
								helper.append(promo)
							}
							
						}
					} else {
						let dateFormatter = DateFormatter()
						dateFormatter.dateFormat = "yyyy-MM-dd"
						let dateStartString = dateFormatter.string(from: start)
						let dateNowString = dateFormatter.string(from: Date())
						
						if dateStartString == dateNowString{
							helper.append(promo)
						}
					}
				}
				
				
				if helper.count > 0 {
					self.promos = helper
				} else {
					print("en helper count")
					EZAlertController.alert("No tenemos promos para mostrarte", message: "")
				}
				
				
			} else {
				EZAlertController.alert("No tenemos promos para mostrarte", message: "")
			}
		})
	}
	
	func pad(string: String, toSize: Int) -> String {
		 var padded = string
		 for _ in 0..<toSize - string.count {
			 padded = "0" + padded
		 }
		 return padded
	 }
    
    // MARK: - UITableView
	
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let promos = self.promos {
            if promos.count > 0 {
                return promos.count
            } else {
                EZAlertController.alert("No tenemos promos para mostrarte", message: "", acceptMessage: "Aceptar", acceptBlock: {
                    if let nav = self.navigationController {
                        nav.popViewController(animated: true)
                    }
                })
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height * 0.55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let promos = self.promos else {
            let cell = UITableViewCell()
            return cell
        }
        
        let promo = promos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "promo-cell", for: indexPath as IndexPath) as! PromoTableViewCell
        cell.title.text = promo.title
        
        if let file = promo.image {
            UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.settingData, completion: { (result, object) in
                switch result {
                case true:
                    let rootImgUrl = object as? String
                    let url = URL(string: "\(rootImgUrl!)\(file.url!)")
                    cell.imagePromo.kf.indicatorType = .activity
                    cell.imagePromo.kf.setImage(with: url)
                case false:
                    cell.imagePromo.backgroundColor = .gray
                }
            })
        } else {
            cell.imagePromo.backgroundColor = .gray
        }
        cell.more.tag = indexPath.row
        return cell
    }
    
    @IBAction func showMore(_ sender: UIButton) {
        guard let promos = self.promos else {
            return
        }
        
        let promo = promos[sender.tag]
        performSegue(withIdentifier: SegueIdentifier.pushToPromoDetail, sender: promo)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.pushToPromoDetail {
            if let promo = sender as? Promo {
                let vc = segue.destination as! PromoDetailViewController
                vc.promo = promo
            }
        }
    }
    
    @IBAction func pushBack(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: Any) {
        pushTo.homeFrom(viewController: self)
    }
}
