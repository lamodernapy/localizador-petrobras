//
//  File.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/12/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation

class File: NSObject, NSCoding {
    var id: String?
    var url: String?
    
    init (dictionary: [String: Any]) {
        id = dictionary["id"] as? String
        url  = dictionary["url"] as? String
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? String
        url  = aDecoder.decodeObject(forKey: "url") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(url, forKey: "url")
    }
}
