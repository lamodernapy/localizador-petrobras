//
//  RallyMapViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 7/11/18.
//  Copyright © 2018 Agencia Lamoderna. All rights reserved.
//

import UIKit
import GoogleMaps

class RallyMapViewController: UIViewController {
    
    var event: Event!

    private var mapView: GMSMapView!
    private var renderer: GMUGeometryRenderer!
    private var kmlParser: GMUKMLParser!

    override func viewDidLoad() {
        super.viewDidLoad()
		
		setNavbarDivider()
        
        let camera = GMSCameraPosition.camera(withLatitude: -23.000, longitude: -58.000, zoom: 6)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.view = mapView

        let split = event.kmz.url?.components(separatedBy: "/")
        let file = split![1].components(separatedBy: ".")
        let fileName = file[0]
        let fileExtension = file[1]

        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent("\(fileName).\(fileExtension)")
        
        kmlParser = GMUKMLParser(url: fileURL)
        kmlParser.parse()

        renderer = GMUGeometryRenderer(map: mapView,
                                       geometries: kmlParser.placemarks,
                                       styles: kmlParser.styles)

        renderer.render()
    }
    
    @IBAction func pushBack(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: Any) {
        pushTo.homeFrom(viewController: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
