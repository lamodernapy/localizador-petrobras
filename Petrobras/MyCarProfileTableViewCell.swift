//
//  MyCarProfileTableViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/4/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import Charts

class MyCarProfileTableViewCell: UITableViewCell {

    @IBOutlet var carImageView: UIImageView!
    @IBOutlet var brandLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var fuelPromLabel: UILabel!
    @IBOutlet var lastFuelLabel: UILabel!
    @IBOutlet var lineChart: LineChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
