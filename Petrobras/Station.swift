//
//  Station.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/5/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation
import CoreLocation

class Station: NSObject, NSCoding {
    var id: String?
    var name: String?
    var region: String?
    var city: String?
    var district: String?
    var address: String?
    var coord: [String: Double]?
    var latitude: Double!
    var longitude: Double!
    var image: String?
    var status: Int?
    var services: [String]?
    var products: [String]?
    
    
    var location: CLLocation {
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    var distance: Double!
    
    init (dictionary: [String: Any]) {
        id = dictionary["id"] as? String
        name = dictionary["name"] as? String
        region = dictionary["region"] as? String
        city = dictionary["city"] as? String
        district = dictionary["district"] as? String
        address = dictionary["address"] as? String
        coord = (dictionary["coords"] as? [String: Double])!
        image = dictionary["image"] as? String
        status = dictionary["status"] as? Int
        services = dictionary["services"] as? [String]
        products = dictionary["fuels"] as? [String]
        latitude = coord?["lat"] != nil ? Double((coord?["lat"])!) : 0
        longitude = coord?["lon"] != nil ? Double((coord?["lon"])!) : 0
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? String
        name  = aDecoder.decodeObject(forKey: "name") as? String
        region = aDecoder.decodeObject(forKey: "region") as? String
        city = aDecoder.decodeObject(forKey: "city") as? String
        district = aDecoder.decodeObject(forKey: "district") as? String
        address = aDecoder.decodeObject(forKey: "address") as? String
        coord = (aDecoder.decodeObject(forKey: "coord") as? [String: Double])!
        image = aDecoder.decodeObject(forKey: "images") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Int
        services = aDecoder.decodeObject(forKey: "services") as? [String]
        products = aDecoder.decodeObject(forKey: "products") as? [String]
        latitude = aDecoder.decodeObject(forKey: "latitude") as? Double
        longitude = aDecoder.decodeObject(forKey: "longitude") as? Double
        distance = aDecoder.decodeObject(forKey: "distance") as? Double
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(region, forKey: "region")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(district, forKey: "district")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(coord, forKey: "coord")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(status, forKey: "status")
        aCoder.encode(services, forKey: "services")
        aCoder.encode(products, forKey: "products")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(distance, forKey: "distance")
    }
    
    // Function to calculate the distance from given location.
    func calculateDistance(fromLocation: CLLocation?) {
        distance = (location.distance(from: fromLocation!) / 1000).roundTo(places: 3)
    }
    
}

extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
