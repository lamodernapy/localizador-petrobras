//
//  IconImage.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/14/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class IconImage: UIView {

    @IBOutlet var icon: UIImageView!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        icon.image = UIImage(named: "icon-chevron")
    }
    

}
