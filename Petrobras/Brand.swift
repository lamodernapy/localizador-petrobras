//
//  Brand.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/5/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation

class Brand: NSObject, NSCoding {
    var id: String!
    var name: String!
    var logo: String!
    var models: [Model] = []
    
    init (dictionary: [String: AnyObject]) {
        id   = dictionary["id"] as? String
        name = dictionary["name"] as? String
        logo = dictionary["logo"] as? String
        if let modelsHelper = dictionary["models"] as? [[String: AnyObject]] {
            for (_, modelDic) in modelsHelper.enumerated() {
                let model = Model(dictionary: modelDic)
                models.append(model)
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        id   = aDecoder.decodeObject(forKey: "id") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        logo = aDecoder.decodeObject(forKey: "logo") as? String
        models = (aDecoder.decodeObject(forKey: "models") as? [Model])!
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(logo, forKey: "logo")
        aCoder.encode(models, forKey: "models")
    }
}
