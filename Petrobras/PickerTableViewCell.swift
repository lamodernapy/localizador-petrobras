//
//  PickerTableViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/6/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class PickerTableViewCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        switch selected {
        case true:
            title.textColor = UIColor.white
            self.accessoryType = .checkmark
        case false:
            title.textColor = UIColor.white
            self.accessoryType = .none
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        switch highlighted {
        case true:
            title.textColor = CustomColors.blue
        case false:
            title.textColor = UIColor.white
        }
    }

}
