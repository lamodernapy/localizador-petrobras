//
//  MyCarProfileViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/1/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import Charts
import EZAlertController

class MyCarProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    var car: Car?
    var avatar: UIImage?
    var fuelDataArray: [FuelData]?
    var maintenanceDataArray: [MaintenanceData]?
    var consumptionAverage: Int?
    
    var lastFuelData: FuelData?
    
    override func viewWillAppear(_ animated: Bool) {
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.carData, completion: { (result, object) in
            switch result {
            case true:
                self.car = object as? Car
            case false:
                EZAlertController.alert("No disponemos de algunos datos.", message: "", acceptMessage: "Aceptar", acceptBlock: {
                    if let nav = self.navigationController {
                        nav.popViewController(animated: true)
                    }
                })
            }
        })
        
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.avatar, completion: { (result, object) in
            switch result {
            case true:
                self.avatar = object as? UIImage
            case false:
                self.avatar = nil
            }
        })
        
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.fuelData, completion: { (result, object) in
            switch result {
            case true:
                print(object)
                self.fuelDataArray = object as? [FuelData]
                print(self.fuelDataArray)
                guard let fuelDataArray = self.fuelDataArray else {
                    return
                }
                
//                let date = fuelDataArray.map { $0.date }
                if fuelDataArray.count > 1 {
                    let consumption = fuelDataArray.map { $0.consumption! }
                    let sum = consumption.reduce(0, +)
                    self.consumptionAverage = sum / consumption.count
                    self.lastFuelData = fuelDataArray.last
                } else {
                    self.lastFuelData = fuelDataArray.first
                }
                
                self.tableView.reloadData()
            case false:
                self.avatar = nil
            }
        })
        
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.maintenanceData, completion: { (result, object) in
            switch result {
            case true:
                if let helper  = object as? [MaintenanceData] {
                    self.maintenanceDataArray = helper.reversed()
                }
                print(self.maintenanceDataArray!)
            case false:
                self.maintenanceDataArray = nil
            }
        })
        
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Mi auto"
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        tableView.register(UINib(nibName: "MaintenanceListTVCell", bundle: nil), forCellReuseIdentifier: "maintenance-list-cell")
        tableView.register(UINib(nibName: "HeaderMaintenance", bundle: nil), forCellReuseIdentifier: "maintenance-header-cell")
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight * 0.1)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        let cell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! MyCarProfileTableViewCell
//        
//        if fuelDataArray != nil && (fuelDataArray?.count)! > 1 {
//            setChart(cell: cell)
//        }
    }
    
    func setChart(cell: MyCarProfileTableViewCell) {
        guard let fuelDataArray = self.fuelDataArray else {
            return
        }
        
        var yValues : [ChartDataEntry] = [ChartDataEntry]()
        var values: [FuelData] = []
        
        if fuelDataArray.count > 20 {
            values = Array(fuelDataArray.suffix(20))
        } else {
            values = fuelDataArray
        }
        
        for i in 0..<values.count {
            let fuelData = values[i]
            yValues.append(ChartDataEntry(x: Double(i + 1), y: Double(fuelData.lts)))
        }
        
        let data = LineChartData()
        let ds = LineChartDataSet(entries: yValues, label: "Lts. cargados")
        
        ds.lineWidth = 3.0
        ds.colors = [CustomColors.yellow]
        ds.mode = .linear
        ds.circleColors = [CustomColors.yellow]
        
        cell.lineChart.data = data
        cell.lineChart.drawGridBackgroundEnabled = false
        cell.lineChart.animate(xAxisDuration: 1.0, easingOption: .easeInElastic)
        cell.lineChart.chartDescription?.text = ""
        cell.lineChart.backgroundColor = .clear
        cell.lineChart.gridBackgroundColor = .white
        cell.lineChart.legend.textColor = .white
        
        let xAxis = cell.lineChart.xAxis
        xAxis.drawLabelsEnabled = false
        xAxis.drawGridLinesEnabled = false
        
        let yAxisRight = cell.lineChart.getAxis(.right)
        yAxisRight.drawLabelsEnabled = false
        yAxisRight.drawGridLinesEnabled = false
        
        let yAxisLeft = cell.lineChart.getAxis(.left)
        yAxisLeft.labelFont = UIFont(name: "TrebuchetMS-Bold", size: 10)!
        yAxisLeft.gridColor = .white
        yAxisLeft.labelTextColor = .white
        
        data.addDataSet(ds)
        cell.lineChart.notifyDataSetChanged()
    }
    
    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            guard let maintenanceArray = maintenanceDataArray else {
                return 0
            }
            
            if maintenanceArray.count < 11 {
                return maintenanceArray.count
            } else {
                return 11
            }
        default:
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            guard let maintenanceArray = maintenanceDataArray else {
                return 0
            }
            
            if maintenanceArray.count > 0 {
                return 50
            } else {
                return 0
            }
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header  = Bundle.main.loadNibNamed("HeaderMaintenance", owner: self, options: nil)?.first as! HeaderMaintenance
        
        switch section {
        case 1:
            header.title.text = "ULTIMOS MANTENIMIENTOS"
            
            return header
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 1:
            return 141
        default:
            switch indexPath.row {
            case 0:
                return 0
            case 1, 3:
                return 70
            case 2:
                if fuelDataArray != nil {
                    if (fuelDataArray?.count)! >= 2 {
                        return 240
                    } else { return 0 }
                } else {
                    return 0
                }
            case 4:
                return 120
            default:
                return 44
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 1:
            if indexPath.row == 10 {
                let footerCell = tableView.dequeueReusableCell(withIdentifier: "maintenance-header-cell", for: indexPath) as! HeaderMaintenance
                footerCell.title.text = "VER TODOS"
                footerCell.title.backgroundColor = CustomColors.blue
                
                return footerCell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "maintenance-list-cell", for: indexPath) as! MaintenanceListTVCell

                guard let maintenanceArray = maintenanceDataArray else {
                    cell.title.text = "No data"
                    cell.cost.isHidden = true
                    return cell
                }
                
                let maintenance = maintenanceArray[indexPath.row]
                
                cell.date.text = FormatDate.event(date: maintenance.date)
                
                cell.title.text = "\(maintenance.maintenance.name!)"
                cell.km.text = "\(format(number: maintenance.km!)) km"
                
                cell.cost.text = "Gs. \(format(number: maintenance.amount!))"
                
                cell.optionButton.tag = indexPath.row
                cell.optionButton.addTarget(self, action: #selector(deleteRow(_:)), for: .touchUpInside)
                
                return cell
            }
        default:
            var cell: MyCarProfileTableViewCell!
            switch indexPath.row {
            case 0:
                cell = tableView.dequeueReusableCell(withIdentifier: "image-cell") as! MyCarProfileTableViewCell
                if avatar != nil {
                    cell.carImageView.image = avatar
                } else {
                    cell.carImageView.backgroundColor = .gray
                }
            case 1:
                cell = tableView.dequeueReusableCell(withIdentifier: "car-data-cell") as! MyCarProfileTableViewCell
                guard let car = self.car else {
                    EZAlertController.alert("No disponemos de algunos datos.", message: "", acceptMessage: "Aceptar", acceptBlock: {
                        if let nav = self.navigationController {
                            nav.popViewController(animated: true)
                        }
                    })
                    return cell
                }
                cell.brandLabel.text = car.brand?.name!
                cell.yearLabel.text = car.year!
                cell.typeLabel.text = car.model?.name!
            case 2:
                cell = tableView.dequeueReusableCell(withIdentifier: "graphic-cell") as! MyCarProfileTableViewCell
                if fuelDataArray != nil && (fuelDataArray?.count)! > 1 {
                    setChart(cell: cell)
                }
            case 3:
                cell = tableView.dequeueReusableCell(withIdentifier: "fuel-data-cell") as! MyCarProfileTableViewCell
                cell.fuelPromLabel.text = fuelDataArray != nil && (fuelDataArray?.count)! > 1 ? "Lts. \(consumptionAverage!)" : "--"
                cell.lastFuelLabel.text = fuelDataArray != nil && (fuelDataArray?.count)! > 0 ?  FormatDate.event(date: (lastFuelData?.date)!) : "--"
            case 4:
                cell = tableView.dequeueReusableCell(withIdentifier: "add-button-cell") as! MyCarProfileTableViewCell
            default:
                break
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                self.performSegue(withIdentifier: SegueIdentifier.pushToEditMyCar, sender: nil)
            }
        default:
            if indexPath.row == 10 {
                self.performSegue(withIdentifier: SegueIdentifier.pushToMaintenanceList, sender: nil)
            }
        }
        
    }
    
    @IBAction func deleteRow(_ sender: UIButton) {
        EZAlertController.actionSheet("Aviso", message: "Desea eliminar este dato", sourceView: self.view, buttons: ["Eliminar", "Cancelar"]) { (action, position) in
            if position == 0 {
                let index = sender.tag
                self.maintenanceDataArray?.remove(at: index)
                if self.maintenanceDataArray?.count == 0 {
                    UserDefaultsClasses.removeObjectWith(key: UserDefaultsKey.maintenanceData)
                } else {
                    UserDefaultsClasses.save(object: self.maintenanceDataArray!, withKey: UserDefaultsKey.maintenanceData)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.pushToEditMyCar {
            let carVC: CreateMyCarViewController = segue.destination as! CreateMyCarViewController
            carVC.edit = true
            carVC.pushBack = true
            carVC.questionPreSave = true
        } else if segue.identifier == SegueIdentifier.pushToMaintenanceList {
            let vc = segue.destination as! MaintenanceListViewController
            vc.maintenanceArray = maintenanceDataArray!
        }
    }
    
    @IBAction func goToMyCarMenu(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "showMyCarMenu", sender: nil)
    }
    
    @IBAction func pushBack(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: AnyObject) {
        pushTo.homeFrom(viewController: self)
    }
    
}
