//
//  SlideMenuViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 11/28/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import EZAlertController

class SlideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
	var promos: [Promo]? = []
    
    let menu = [
        ["image": "icon-slidemenu-nearby",
         "text": "Estaciones más próximas"],
        ["image": "icon-slidemenu-map",
         "text": "Estaciones por ciudad"],
        ["image": "icon-slidemenu-search",
         "text": "Búsqueda por productos y servicios"],
        ["image": "icon-slidemenu-fuel",
         "text": "Mi consumo"],
        ["image": "icon-slidemenu-gas",
         "text": "Control de mi garrafa"],
        ["image": "icon-slidemenu-promos",
         "text": "Novedades y Promociones"],
        ["image": "icon-slidemenu-contact",
         "text": "Contacto"]
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
    }

    // MARK: - UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height / 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "slide-menu-cell", for: indexPath as IndexPath) as! SlideMenuTableViewCell
        let menu = self.menu[indexPath.item]
        cell.title.text = menu["text"]
        cell.icon.image = UIImage(named: menu["image"]!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let stationsData = UserDefaults.standard.object(forKey: UserDefaultsKey.nearestStationData) as? Data
            if stationsData != nil {
                let stations = NSKeyedUnarchiver.unarchiveObject(with: stationsData!) as! [Station]
                performSegue(withIdentifier: SegueIdentifier.pushToStationList, sender: stations)
            } else {
                EZAlertController.alert("No tenemos los datos de las estaciones mas cercanas")
            }
        case 1:
            performSegue(withIdentifier: SegueIdentifier.pustToCityStation, sender: nil)
        case 2:
            performSegue(withIdentifier: SegueIdentifier.pushToCustomSearch, sender: nil)
        case 3:
            let data = UserDefaults.standard.object(forKey: UserDefaultsKey.carData) as? Data
            if data != nil {
                self.performSegue(withIdentifier: SegueIdentifier.pushToMyCarProfile, sender: nil)
            } else {
                self.performSegue(withIdentifier: SegueIdentifier.pushToCreateMyCar, sender: nil)
            }
        case 4:
            self.performSegue(withIdentifier: SegueIdentifier.pushToGasCylinder, sender: nil)
        case 5:
            self.performSegue(withIdentifier: SegueIdentifier.pushToPromo, sender: nil)
        case 6:
            self.performSegue(withIdentifier: SegueIdentifier.pushToContact, sender: nil)
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func setNavigationBar() {
        let iconBrand = UIImageView(frame: CGRect(x: 15, y: -10, width: 170, height: 50))
        iconBrand.image = UIImage(named: "logo")
        let navigationView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 30))
        navigationView.addSubview(iconBrand)
        self.navigationItem.titleView = navigationView
    }
    
    @IBAction func closeSlideMenu(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
	
	func pad(string: String, toSize: Int) -> String {
		var padded = string
		for _ in 0..<toSize - string.count {
			padded = "0" + padded
		}
		
		return padded
	 }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.pushToStationList {
            let vc: StationListViewController = segue.destination as! StationListViewController
            vc.stations = sender as! [Station]
		}
    }

}
