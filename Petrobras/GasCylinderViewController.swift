
//
//  GasCylinderViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/15/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import EZAlertController
import DatePickerDialog

class GasCylinderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var averageLabel: UILabel!
    @IBOutlet var lastLabel: UILabel!
    
    var gasArrayData: [GasData]?
    
    override func viewWillAppear(_ animated: Bool) {
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.gasData, completion: { (result, object) in
            switch result {
            case true:
                self.gasArrayData = object as? [GasData]
                self.reloadLabel()
            case false:
                self.gasArrayData = nil
                self.reloadLabel()
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		setNavbarDivider()
        
        self.title = "Garrafa"
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height * 0.6)
        tableView.tableFooterView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height * 0.2)
    }

    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let gasArrayData = self.gasArrayData else {
            return 0
        }
        
        switch gasArrayData.count {
        case 0:
            return 0
        default:
            return gasArrayData.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 50
        default:
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "title-cell")
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "date-cell") as! GasTableViewCell
            
            guard let gasArrayData = self.gasArrayData else {
                return cell
            }
            
            let gasData = gasArrayData[gasArrayData.count - indexPath.row]
            cell.dateLabel.text = gasData.date != nil ? FormatDate.event(date: gasData.date) : "--"
            
            cell.optionButton.tag = indexPath.row - 1
            cell.optionButton.addTarget(self, action: #selector(deleteRow(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    @IBAction func deleteRow(_ sender: UIButton) {
        EZAlertController.actionSheet("Aviso", message: "Desea eliminar este dato", sourceView: self.view, buttons: ["Eliminar", "Cancelar"]) { (action, position) in
            if position == 0 {
                let index = sender.tag
                self.gasArrayData?.remove(at: index)
                if self.gasArrayData?.count == 0 {
                    UserDefaultsClasses.removeObjectWith(key: UserDefaultsKey.gasData)
                } else {
                    UserDefaultsClasses.save(object: self.gasArrayData!, withKey: UserDefaultsKey.gasData)
                }
                
                self.reloadLabel()
                self.tableView.reloadData()
            }
        }
    }
    
    func reloadLabel() {
        guard let gasArrayData = self.gasArrayData else {
            averageLabel.text = "--"
            lastLabel.text = "--"
            return
        }
        
        guard gasArrayData.count != 0 else {
            averageLabel.text = "--"
            lastLabel.text = "--"
            return
        }
        
        var deltaArray: [Int] = []
        for (i, gasData) in gasArrayData.enumerated() {
            if i + 1 < gasArrayData.count {
                let nextGasData = gasArrayData[i+1]
                let delta = daysBetweenDates(startDate: gasData.date, endDate: nextGasData.date)
                deltaArray.append(delta)
            }
        }
        
        let sum = deltaArray.reduce(0, +)
        let average = sum != 0 ? sum / deltaArray.count : 0
        averageLabel.text = average != 0 ? "\(average)" : "--"
        
        if let lastGasData = gasArrayData.last {
            lastLabel.text = lastGasData.date != nil ? FormatDate.event(date: lastGasData.date) : "--"
        }
    }
    
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
    
    @IBAction func generateGasData(_ sender: Any) {
        DatePickerDialog().show("Seleccione la fecha", doneButtonTitle: "Aceptar", cancelButtonTitle: "Cancelar", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { (date) in
            guard let dateSelected = date else {
                return
            }
            
            let gasData = GasData(date: dateSelected)
            
            UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.gasData, completion: { (result, object) in
                switch result {
                case true:
                    guard let gasArrayData = object as? [GasData] else {
                        return
                    }
                    
                    var dataHelper = gasArrayData
                    dataHelper.append(gasData)
                    dataHelper.sort(by: { $0.date < $1.date })
                    
                    self.gasArrayData?.append(gasData)
                    self.gasArrayData?.sort(by: { $0.date < $1.date })
                    
                    UserDefaultsClasses.save(object: dataHelper, withKey: UserDefaultsKey.gasData)
                case false:
                    var dataHelper: [GasData] = []
                    dataHelper.append(gasData)
                    dataHelper.sort(by: { $0.date < $1.date })
                    
                    self.gasArrayData = dataHelper
                    self.gasArrayData?.sort(by: { $0.date < $1.date })
                    
                    UserDefaultsClasses.save(object: dataHelper, withKey: UserDefaultsKey.gasData)
                }
                self.reloadLabel()
                self.tableView.reloadData()
            })
        }
    }
    
    @IBAction func callPetroGas(_ sender: Any) {
        let call = UIAlertAction(title: "Llamar al 021 618 1000", style: .default, handler: { (UIAlertAction) -> Void in
            if let url = URL(string: "tel://0216181000") {
                UIApplication.shared.openURL(url)
            }
        })
        
        let cancel = UIAlertAction(title: "Cancelar", style: .destructive, handler: { (UIAlertAction) -> Void in })
        EZAlertController.actionSheet("Confirmar llamada", message: "Desea comunicarse con nosotros?", sourceView: self.view, actions: [call, cancel])
    }
    
    // MARK: - Navigation
    
    @IBAction func pushBack(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }

    @IBAction func pushToHome(_ sender: AnyObject) {
        pushTo.homeFrom(viewController: self)
    }
}
