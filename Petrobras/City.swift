//
//  City.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/13/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation

class City: NSObject, NSCoding {
    var id: String?
    var name: String?
    
    init (dictionary: [String: AnyObject]) {
        id = dictionary["id"] as? String
        name  = dictionary["name"] as? String
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? String
        name  = aDecoder.decodeObject(forKey: "name") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
    }
}
