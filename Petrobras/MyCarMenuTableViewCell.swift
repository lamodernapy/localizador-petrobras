//
//  MyCarMenuTableViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/2/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class MyCarMenuTableViewCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureWith(data: [String: String]) {
        title.text = data["text"]
        
        if data["icon"] != nil {
            icon.image = UIImage(named: data["icon"]!)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
