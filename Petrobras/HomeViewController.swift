//
//  HomeViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 11/24/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import CoreLocation
import EZAlertController
import AlamofireImage
import MapKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, CLLocationManagerDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var menuContentView: UIView!
    @IBOutlet var menuView: UICollectionView!
    
    let menu = [
        ["image": "icon-rally",
         "text": "Campeonato Nacional Petrobras Rally"],
        ["image": "icon-menu-gas-station",
         "text": "Estaciones de servicios"],
        ["image": "icon-menu-gas",
         "text": "Gas garrafa"],
        ["image": "icon-menu-promos",
        "text": "Novedades y Promociones"]
    ]
    
    var locationManager: CLLocationManager!
    var stations: [Station]?
    var nearestStations: [Station]?
    var services: [Service]?
    var products: [Product]?
    
    override func viewWillAppear(_ animated: Bool) {
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.serviceData, completion: { (result, object) in
            if result == true {
                self.services = object as? [Service]
            }
        })
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.stationsData, completion: {(result, object) in
            if result == true {
                self.stations = object as? [Station]
            }
        })
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.productsData, completion: {(result, object) in
            if result == true {
                self.products = object as? [Product]
            }
        })
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
		setNavbarDivider()
        
		tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: view.safeAreaHeight)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: ScreenSize.width / 2, height: (ScreenSize.height - 100 - 64) / 2)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        menuView!.collectionViewLayout = layout
        
        locationManager = CLLocationManager()
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            locationManager.startUpdatingLocation()
        }
        
        tableView.register(UINib(nibName: "StationCell", bundle: nil), forCellReuseIdentifier: "station-cell")
    }
    
    // MARK: - UICollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menu-cell", for: indexPath as IndexPath) as! MenuCollectionViewCell
        let menu = self.menu[indexPath.item]
        cell.title.text = menu["text"]
        cell.imageView.image = UIImage(named: menu["image"]!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            self.performSegue(withIdentifier: SegueIdentifier.pushToEventList, sender: nil)
        case 1:
            let stationsData = UserDefaults.standard.object(forKey: UserDefaultsKey.nearestStationData) as? Data
            if stationsData != nil {
                let stations = NSKeyedUnarchiver.unarchiveObject(with: stationsData!) as! [Station]
                performSegue(withIdentifier: SegueIdentifier.pushToStationList, sender: stations)
            } else {
                EZAlertController.alert("No tenemos los datos de las estaciones mas cercanas")
            }
        case 2:
            self.performSegue(withIdentifier: SegueIdentifier.pushToGasCylinder, sender: nil)
        case 3:
            UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.promoData, completion: { (result, object) in
                if result == true {
                    let promos = object as? [Promo]
                    
                    guard let promosHelper = promos else {
                        EZAlertController.alert("No tenemos promos para mostrarte", message: "")
                        return
                    }
                    
                    let currentDate = Date()
                    let myCalendar = Calendar(identifier: .gregorian)
                    let weekDay = myCalendar.component(.weekday, from: currentDate) - 1
                    
                    var helper: [Promo] = []
                    
                    for (_, promo) in promosHelper.enumerated() {
                        guard
                            let start = promo.startDate,
                            let end = promo.finishDate
                        else {
                            return
                        }
                        
						if currentDate >= start && currentDate <= end {
                            let binaryString = String(promo.days, radix: 2)
                            let binaryPad = self.pad(string: binaryString, toSize: 7)
                            
							let reverseBinaryPad = binaryPad.reversed()
                            
                            for (i, value) in reverseBinaryPad.enumerated() {
                                let character = String(value)
                                
                                if i == weekDay && character == "1" {
                                    print("agrega")
                                    helper.append(promo)
                                }
                                
                            }
                        } else {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            let dateStartString = dateFormatter.string(from: start)
                            let dateNowString = dateFormatter.string(from: Date())
                            
                            if dateStartString == dateNowString {
                                helper.append(promo)
                            }
                        }
                    }
                    
                    
                    if helper.count > 0 {
                        self.performSegue(withIdentifier: SegueIdentifier.pushToPromo, sender: helper)
                    } else {
                        print("en helper count")
                        EZAlertController.alert("No tenemos promos para mostrarte", message: "")
                    }
                    
                    
                } else {
                    EZAlertController.alert("No tenemos promos para mostrarte", message: "")
                }
            })
        default:
            break
        }
        
        menuView.deselectItem(at: indexPath, animated: true)
    }
    
    func pad(string: String, toSize: Int) -> String {
        var padded = string
        for _ in 0..<toSize - string.count {
            padded = "0" + padded
        }
        return padded
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! MenuCollectionViewCell
        let overlay = UIView(frame: cell.imageView.bounds)
        overlay.tag = 123
        overlay.backgroundColor = CustomColors.green.withAlphaComponent(0.5)
        overlay.layer.cornerRadius = overlay.frame.height / 2
        cell.imageView.addSubview(overlay)
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! MenuCollectionViewCell
        let overlay = cell.contentView.viewWithTag(123)
        overlay?.removeFromSuperview()
    }
    
    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let stations = nearestStations else {
            return 1
        }
        
        switch stations.count != 0 {
        case true:
            return 11
        case false:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let stations = nearestStations else {
            return 100
        }
        
        if stations.count != 0 {
            switch indexPath.row {
            case 0:
                return 100
            case 1:
                return 50
            default:
                return 250
            }
        }
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let stations = nearestStations else {
            let cell = UITableViewCell()
            return cell
        }
        
        switch stations.count != 0 {
        case true:
            let station = stations[indexPath.row]
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "next-station-cell", for: indexPath) as! HeaderStationTableViewCell
                cell.name.text = station.name
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "title-cell")
                return cell!
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "station-cell", for: indexPath) as! StationTableViewCell
                
                var products: [Product] = []
                for productHelper in station.products! {
                    let product = self.products?.first(where: { productHelper == $0.id })
                    products.append(product!)
                }
                cell.products = products
                if let servicesHelper = self.services {
                    let services = servicesHelper.sorted(by: { $0.priority! > $1.priority! })
                    cell.services = services
                }
                cell.station = station
                return cell
            }
        case false:
            let cell = tableView.dequeueReusableCell(withIdentifier: "next-station-cell", for: indexPath) as! HeaderStationTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            return
        default:
            Loader.start()
            guard let stations = nearestStations else {
                EZAlertController.alert("Atención", message: "No tenemos autorización para acceder a tu ubicación.")
                return
            }
            let station = stations[indexPath.row]
            openNavigation(forStation: station)
            Loader.stop()
        }
    }
    
    func openNavigation (forStation: Station) {
        let maps = UIAlertAction(title: "Maps", style: .default, handler: { (UIAlertAction) -> Void in
            let coordinate = CLLocationCoordinate2DMake(forStation.latitude, forStation.longitude)
            let region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.01, 0.02))
            let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: region.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: region.span)]
            mapItem.name = forStation.name
            mapItem.openInMaps(launchOptions: options)
        })
        
        let gMaps = UIAlertAction(title: "Google maps", style: .default, handler: { (UIAlertAction) -> Void in
            let latitude = String(forStation.latitude)
            let longitude = String(forStation.longitude)
            
            UIApplication.shared.openURL(URL(string: "comgooglemaps://??saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!)
        })
        
        let cancel = UIAlertAction(title: "Cancelar", style: .destructive, handler: { (UIAlertAction) -> Void in
            print("Second Button pressed")
        })
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            EZAlertController.actionSheet("Alerta!", message: "Cuál aplicación desea abrir?", sourceView: self.view, actions: [maps, gMaps, cancel])
        } else {
            EZAlertController.actionSheet("Alerta!", message: "Desea abrir?", sourceView: self.view, actions: [maps, cancel])
        }
    }
    
    // MARK - Location manager Delegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .authorizedWhenInUse {
            EZAlertController.alert("Atención", message: "No tenemos autorización para acceder a tu ubicación.")
        }
        tableView.reloadData()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        Loader.start()
        let userLocation: CLLocation = manager.location!
        guard let stations = self.stations else {
            EZAlertController.alert("Atención", message: "No tenemos los datos de las estaciones.")
            Loader.stop()
            return
        }
        
        for station in stations {
            station.calculateDistance(fromLocation: userLocation)
        }
        
        nearestStations = stations.sorted(by: {
            $0.distance < $1.distance
        })
        
        UserDefaultsClasses.save(object: nearestStations!, withKey: UserDefaultsKey.nearestStationData)
        Loader.stop()
        tableView.reloadData()
    }

    // MARK: - Navigation Bar
    
    func setNavigationBar() {
        let iconBrand = UIImageView(frame: CGRect(x: 15, y: -10, width: 170, height: 50))
        iconBrand.image = UIImage(named: "logo")
        let navigationView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 30))
        navigationView.addSubview(iconBrand)
        self.navigationItem.titleView = navigationView
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.pushToStationList {
            let vc: StationListViewController = segue.destination as! StationListViewController
			vc.stations = sender as? [Station]
        } else if segue.identifier == SegueIdentifier.pushToPromo {
            let vc = segue.destination as! PromoViewController
            vc.promos = sender as? [Promo]
        }
    }
}
