//
//  CustomSearchViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 11/28/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import Kingfisher
import EZAlertController

class CustomSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var menuContentView: UIView!
    @IBOutlet var menuView: UICollectionView!

    var services: [Service]!
    var products: [Product]!
    var stations: [Station]!
    
    var servicesId: [String] = []
    var productsId: [String] = []
    
    let defaults = UserDefaults.standard
    
    override func viewWillAppear(_ animated: Bool) {
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        let serviceData = defaults.object(forKey: UserDefaultsKey.serviceData) as! Data
        services = NSKeyedUnarchiver.unarchiveObject(with: serviceData) as! [Service]
        services = services.sorted(by: { $0.priority! > $1.priority! })
        
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: ((screenWidth / 3) * CGFloat(((services.count / 3) + 2)) + 50))
        
        let productsData = defaults.object(forKey: UserDefaultsKey.productsData) as! Data
        products = NSKeyedUnarchiver.unarchiveObject(with: productsData) as! [Product]
        products = products.sorted(by: { $0.priority! > $1.priority! })
        
        let stationsData = defaults.object(forKey: UserDefaultsKey.nearestStationData) as! Data
        stations = NSKeyedUnarchiver.unarchiveObject(with: stationsData) as! [Station]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		setNavbarDivider()
        
        self.title = "Búsqueda"
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: screenWidth / 3, height: screenWidth / 3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.headerReferenceSize = CGSize(width: 0, height: 100)
        layout.footerReferenceSize = CGSize(width: 0, height: 50)
        menuView!.collectionViewLayout = layout
        menuView.allowsMultipleSelection = true
        
        
        tableView.tableFooterView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight * 0.2)
    }
    
    // MARK: - UICollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return services.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menu-cell", for: indexPath as IndexPath) as! MenuCollectionViewCell
        let service = services[indexPath.item]
        cell.title.text = service.name
        
        if let file = service.image {
            UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.settingData, completion: { (result, object) in
                switch result {
                case true:
                    let rootImgUrl = object as? String
                    let url = URL(string: "\(rootImgUrl!)\(file.url!)")
                    cell.imageView.kf.indicatorType = .activity
                    cell.imageView.kf.setImage(with: url)
                case false:
                    break
                }
            })
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var view: UICollectionReusableView = UICollectionReusableView()
        switch kind {
        case UICollectionElementKindSectionHeader:
            view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as UICollectionReusableView
            return view
        case UICollectionElementKindSectionFooter:
            view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footer", for: indexPath) as UICollectionReusableView
            return view
        default:
            break
        }
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = collectionView.cellForItem(at: indexPath) as? MenuCollectionViewCell
        item?.setSelected(true, animated: true)
        let service = services[indexPath.item]
        servicesId.append(service.id!)
        print(servicesId)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let item = collectionView.cellForItem(at: indexPath) as? MenuCollectionViewCell
        item?.setSelected(false, animated: true)
        if servicesId.count != 0 {
            let service = services[indexPath.item]
            servicesId = servicesId.filter{ $0 != service.id }
        }
        print(servicesId)
        
//        removeIndex(index: indexPath.row, key: "services-selected")
    }
    
    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "product-cell")! as! ProductTableViewCell
        let product = products[indexPath.row]
        
        cell.title.text = product.name
        cell.icon.image = UIImage(named: "icon-checkon-blue")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("select")
//        let cell = tableView.cellForRow(at: indexPath) as? ProductTableViewCell
        let product = products[indexPath.row]
        productsId.append(product.id!)
        print(productsId)
//        cell?.setSelected(true, animated: true, data: product)
//        saveIndex(index: indexPath.row, key: "products-selected")
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("deselect")
//        let cell = tableView.cellForRow(at: indexPath) as? ProductTableViewCell
        if productsId.count != 0 {
            let product = products[indexPath.item]
            productsId = productsId.filter{ $0 != product.id }
        }
        print(productsId)
//        cell?.setSelected(false, animated: true, data: product)
//        removeIndex(index: indexPath.row, key: "products-selected")
    }
    
    // MARK: - Save user selections
    
    func saveIndex(index: Int, key: String) {
        if var cellSelected = defaults.array(forKey: key) as? [Int] {
            print(cellSelected)
            cellSelected.append(index)
            print(cellSelected)
            defaults.set(cellSelected, forKey: key)
        } else {
            var cellSelected: [Int] = []
            cellSelected.append(index)
            defaults.set(cellSelected, forKey: key)
        }
    }
    
    func removeIndex(index: Int, key: String) {
        if var cellSelected = defaults.array(forKey: key) as? [Int] {
            cellSelected = cellSelected.filter(){ $0 != index }
            print(cellSelected)
            defaults.set(cellSelected, forKey: key)
        }
    }
    
    func checkIndex(index: Int, key: String) -> Bool {
        if let cellSelected = defaults.array(forKey: key) as? [Int] {
            return cellSelected.contains(index) ? true : false
        }
        
        return false
    }

    
    @IBAction func goToStationList(_ sender: Any) {
        if servicesId.count != 0 || productsId.count != 0 {
            var filteredStation: [Station] = []
            for station in stations {
                let serviceIdSet = Set(servicesId)
                let stationServiceSet = Set(station.services!)
                
                let productIdSet = Set(productsId)
                let stationProductSet = Set(station.products!)
                
                if serviceIdSet.isSubset(of: stationServiceSet) == true
                    && productIdSet.isSubset(of: stationProductSet) == true {
                    filteredStation.append(station)
                }
            }
            
            if filteredStation.count != 0 {
                performSegue(withIdentifier: SegueIdentifier.pushToStationList, sender: filteredStation)
            } else {
                EZAlertController.alert("Error", message: "No se han encontrado estaciones!")
            }
        } else {
            EZAlertController.alert("Error", message: "Elija al menos alguna opción.")
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.pushToStationList {
            let vc = segue.destination as? StationListViewController
            vc?.stations = sender as? [Station]
        }
    }
    
    @IBAction func pushBack(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: Any) {
        pushTo.homeFrom(viewController: self)
    }
    
}
