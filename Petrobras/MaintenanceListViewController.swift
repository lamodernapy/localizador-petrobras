//
//  MaintenanceListViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 2/13/17.
//  Copyright © 2017 Agencia Lamoderna. All rights reserved.
//

import UIKit
import EZAlertController

class MaintenanceListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    var maintenanceArray: [MaintenanceData]!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Mantenimientos"
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        tableView.register(UINib(nibName: "MaintenanceListTVCell", bundle: nil), forCellReuseIdentifier: "maintenance-list-cell")
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height * 0.1)
    }
    
    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return maintenanceArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 141
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "maintenance-list-cell", for: indexPath) as! MaintenanceListTVCell
        let maintenance = maintenanceArray[indexPath.row]
        
        cell.date.text = FormatDate.event(date: maintenance.date)
        
        cell.title.text = "\(maintenance.maintenance.name!)"
        cell.km.text = "\(format(number: maintenance.km!)) km"
        
        cell.cost.text = "Gs. \(format(number: maintenance.amount!))"
        
        cell.optionButton.tag = indexPath.row
        cell.optionButton.addTarget(self, action: #selector(deleteRow(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @IBAction func deleteRow(_ sender: UIButton) {
        EZAlertController.actionSheet("Aviso", message: "Desea eliminar este dato", sourceView: self.view, buttons: ["Eliminar", "Cancelar"]) { (action, position) in
            if position == 0 {
                let index = sender.tag
                self.maintenanceArray.remove(at: index)
                UserDefaultsClasses.save(object: self.maintenanceArray, withKey: UserDefaultsKey.maintenanceData)
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Navigation
    
    @IBAction func pushBack(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: AnyObject) {
        pushTo.homeFrom(viewController: self)
    }

}
