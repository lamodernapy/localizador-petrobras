//
//  Regions.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/13/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation

class Region: NSObject, NSCoding {
    var id: String?
    var name: String?
    var cities: [City] = []
    
    init (dictionary: [String: Any]) {
        id = dictionary["id"] as? String
        name  = dictionary["name"] as? String
        
        if let citiesHelper = dictionary["cities"] as? [[String: AnyObject]] {
            for cityDic in citiesHelper {
                let city = City(dictionary: cityDic)
                cities.append(city)
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? String
        name  = aDecoder.decodeObject(forKey: "name") as? String
        cities = aDecoder.decodeObject(forKey: "cities") as! [City]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(cities, forKey: "cities")
    }
}
