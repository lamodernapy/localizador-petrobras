//
//  EventListViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 7/5/18.
//  Copyright © 2018 Agencia Lamoderna. All rights reserved.
//

import UIKit

class EventListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var events: [Event]?
    
    override func viewWillAppear(_ animated: Bool) {
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.eventsData, completion: { (result, object) in
            if result == true {
                self.events = object as? [Event]
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		setNavbarDivider()
        
        self.title = "Lista de Eventos"
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight * 0.15)
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
        tableView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            let identifier = segue.identifier,
            let vc = segue.destination as? EventDetailViewController,
            let event = sender as? Event
        else {
            return
        }
        vc.event = event
    }
    
    @IBAction func pushBack(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: Any) {
        pushTo.homeFrom(viewController: self)
    }

}

extension EventListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let events = events else {
            return 1
        }
        return events.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
        cell.backgroundColor = .clear
        guard let events = events else {
            return UITableViewCell()
        }
        let event = events[indexPath.row]
        cell.date.text = "\(event.date!)"
        cell.title.text = event.title
        cell.content.text = event.desc
        return cell
    }
}

extension EventListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let events = events else {
            return
        }
        let event = events[indexPath.row]
        self.performSegue(withIdentifier: SegueIdentifier.pushToEventDetail, sender: event)
    }
}
