//
//  Extensions.swift
//  Petrobras
//
//  Created by Felix on 9/28/20.
//  Copyright © 2020 Agencia Lamoderna. All rights reserved.
//

import Foundation

extension UIViewController {
	func setNavbarDivider() {
		let image = UIImage(named: "background-shadow-navigation-bar")
		let imageView = UIImageView(image: image)
		imageView.frame = CGRect(x: 0, y: -40, width: view.frame.width, height: 80)
		
		view.addSubview(imageView)
		view.bringSubview(toFront: imageView)
	}
}

extension UIView {
	var safeAreaHeight: CGFloat {
		if #available(iOS 11, *) {
			return safeAreaLayoutGuide.layoutFrame.size.height
		}
		
		return bounds.height
	}
}
