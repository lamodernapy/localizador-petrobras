//
//  PromoTableViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/16/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class PromoTableViewCell: UITableViewCell {
    
    @IBOutlet var wrapper: UIView!
    @IBOutlet var title: UILabel!
    @IBOutlet var imagePromo: UIImageView!
    @IBOutlet var more: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
