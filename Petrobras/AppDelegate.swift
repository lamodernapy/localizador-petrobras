
//  AppDelegate.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 11/24/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
import IQKeyboardManagerSwift
import GoogleMaps
import Alamofire
import SwiftyJSON
import TwitterKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var develop = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.userData) { (result, data) in
            print("exist user: \(result)")
            switch result {
            case true:
                UserRequest.checkSession({ (result) in
                    print("session: \(result)")
                    switch result {
                    case true:
                        self.firebaseStart(application)
                    case false:
                        let user = data as? User
                        UserRequest.createSession(user!, completion: { (result) in
                            print("session created: \(result)")
                            switch result {
                            case true:
                                self.firebaseStart(application)
                            case false:
                                break
                            }
                        })
                    }
                })
            case false:
                var imei = UIDevice.current.identifierForVendor!.uuidString
                imei = imei.replacingOccurrences(of: "-", with: "")
                print(imei)
                
                UserRequest.register(phoneId: imei) { (result) in
                    print("register user: \(result)")
                    switch result {
                    case true:
                        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.userData) { (result, data) in
                            if result == true {
                                let user = data as? User
                                UserRequest.createSession(user!, completion: { (result) in
                                    switch result {
                                    case true:
                                        self.firebaseStart(application)
                                    case false:
                                        break
                                    }
                                })
                            }
                        }
                    case false:
                        break
                    }
                }
            }
        }
        
//        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
//        UINavigationBar.appearance().isTranslucent = true
        
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        GMSServices.provideAPIKey("AIzaSyCVtCsxAFTBS-90fkD1sB8WACB_VkVv3Zs")
        
        TWTRTwitter.sharedInstance().start(withConsumerKey:"87GqDLGWVPpt7fZDIVrVBWNjz", consumerSecret:"jmTUdRGvKEV6PERXKeD09ox1iGIgR2IzzrB11dZZ03TSCQIqeJ")
        
        return true
    }
    
    func firebaseStart(_ application: UIApplication) {
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_,_ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // MARK: - Fonts
    
    func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        guard
            let action = userInfo["action"] as? String,
            let dataHelper = userInfo["promo"] as? String
            else {
                return
        }
        
        guard action == "promo" else {
            return
        }
        
        guard let dataFromString = dataHelper.data(using: .utf8, allowLossyConversion: false) else {
            return
        }
        
        let promoData = try! JSON(data: dataFromString)
        
        let promoId = promoData["id"].stringValue
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LauncherVC") as! LauncherViewController
        vc.notification = true
        vc.promoId = promoId
        
        let nvc: UINavigationController = UINavigationController(rootViewController: vc)
        nvc.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        nvc.navigationBar.shadowImage = UIImage()
        nvc.navigationBar.isTranslucent = true
        nvc.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        self.window?.rootViewController = nvc
        self.window?.makeKeyAndVisible()
        
        completionHandler()
        
        completionHandler()
    }
}
// [END ios_10_message_handling]

extension AppDelegate: MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        // if let appToken = UserDefaults.standard.value(forKey: Helper.UserDefaultsKey.token.rawValue) as? String {
        //    APIHelper.registerFCM(appToken: appToken, FCMToken: fcmToken)
        //}
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

