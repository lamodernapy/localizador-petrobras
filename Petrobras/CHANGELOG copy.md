# Change Log
Todos los cambios notables para este proyecto deben ser documentados en este archivo.
Este proyecto sigue las convenciones del popular [Versionado Semántico](http://semver.org/) y de [Keep a CHANGELOG](http://keepachangelog.com/).

## [1.0] [1.0.1] - 2017-01-12
### Fixed
- Corrección de errores menores 
- Se eliminaron todos los features relativo al uso de la cámara y galería de imagenes

## [1.0] [1.0.0] - 2017-01-02
### Added
- Implementación Firebase.

## [1.0] [1.0.0] - 2016-12-21
### Added
- Versión 1.0 enviada para su verificicación.    

## [1.0] [1.0.0] - 2016-11-24
### Added
- Initial Commit.
