//
//  MyCarTableViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 11/30/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class MyCarTableViewCell: UITableViewCell {

    @IBOutlet var wrapper: UIView!
    @IBOutlet var titleOption: UILabel!
    @IBOutlet var selectedOption: UILabel!
    @IBOutlet var icon: UIImageView!
    @IBOutlet var textfield: UITextField!
    @IBOutlet var message: UILabel!
    @IBOutlet var selectedDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureWith(data: [String: String]) {
        titleOption.text = data["text"]
        if let iconString = data["icon"] {
            if iconString.isEmpty == false {
                icon.image = UIImage(named: iconString)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        switch highlighted  {
        case true:
            let overlay = UIView(frame: wrapper.bounds)
            overlay.tag = 123
            overlay.backgroundColor = CustomColors.green.withAlphaComponent(0.3)
            overlay.clipsToBounds = true
            wrapper.addSubview(overlay)
        case false:
            let overlay = super.viewWithTag(123)
            overlay?.removeFromSuperview()
        }
        
    }
    
    

}
