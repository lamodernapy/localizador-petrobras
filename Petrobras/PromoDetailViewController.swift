//
//  PromoDetailViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/16/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import WebKit

class PromoDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, WKNavigationDelegate {
    
    @IBOutlet var tableView: UITableView!
	var promo: Promo!
    var dynamicHeight: CGFloat = 0.0
    var firstReload: Bool = false
    var notification = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		setNavbarDivider()
		
        self.title = "Novedades y Promociones"
        
        if notification == false {
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"icon-nav-back"), style: .plain, target: self, action: #selector(pushBack(_:)))
        } else {
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"icon-close"), style: .plain, target: self, action: #selector(close(_:)))
            navigationItem.rightBarButtonItem = nil
        }
        
        navigationItem.leftBarButtonItem?.tintColor = CustomColors.blue
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        tableView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return tableView.frame.size.height * 0.4
        case 1:
            return 60
        case 2:
            return dynamicHeight + 100
        default:
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "image-cell") as! PromoDetailTableViewCell
            
            if promo.image != nil {
                if let file = promo.image {
                    UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.settingData, completion: { (result, object) in
                        switch result {
                        case true:
                            let rootImgUrl = object as? String
                            let url = URL(string: "\(rootImgUrl!)\(file.url!)")
                            cell.imagePromo.kf.indicatorType = .activity
                            cell.imagePromo.kf.setImage(with: url)
                        case false:
                            cell.imagePromo.backgroundColor = .gray
                        }
                    })
                } else {
                    cell.imagePromo.backgroundColor = .gray
                }
            } else {
                if let url = promo.imageUrl {
                    UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.settingData, completion: { (result, object) in
                        switch result {
                        case true:
                            let rootImgUrl = object as? String
                            let url = URL(string: "\(rootImgUrl!)\(url)")
                            cell.imagePromo.kf.indicatorType = .activity
                            cell.imagePromo.kf.setImage(with: url)
                        case false:
                            cell.imagePromo.backgroundColor = .gray
                        }
                    })
                } else {
                    cell.imagePromo.backgroundColor = .gray
                }
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "title-cell") as! PromoDetailTableViewCell
            cell.title.text = promo.title
            cell.title.textColor = CustomColors.blue
            return cell
        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "description-webview-cell") as! PromoDetailTableViewCell
            let html = htmlFromBodyString(htmlBodyString: promo.descriptionPromo, textFont: UIFont(name: "TrebuchetMS", size: 17)!, textColor: CustomColors.blue)
			cell.descriptionView.navigationDelegate = self
            cell.descriptionView.loadHTMLString(html, baseURL: nil)
            
            return cell
        default:
            let cell = UITableViewCell()
            return cell
        }
    }
    
    func htmlFromBodyString(htmlBodyString: String, textFont: UIFont, textColor: UIColor) -> String {
        let colorHexString = textColor.hexString
        
        let html = "<html>\n <head>\n <style type=\"text/css\">\n body {word-wrap: break-word; font-family: \"\(textFont.familyName)\"; font-size: \(textFont.pointSize)px; color:\(colorHexString);}\n </style>\n </head>\n <body>\(htmlBodyString)</body>\n </html>"
        
        return html
    }
	
	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
		if firstReload == false {
			let cell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! PromoDetailTableViewCell
			
			var f = cell.descriptionView.frame
			f.size.height = webView.scrollView.contentSize.height
			
			cell.descriptionView.scrollView.isScrollEnabled = false
			cell.descriptionView.scrollView.frame = cell.descriptionView.frame
			
			dynamicHeight = f.size.height
			
			tableView.reloadData()
			
			if dynamicHeight > 60 {
				firstReload = true
			}
		}
	}
    
    private func webView(_ webView: WKWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if firstReload == true {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(request.url!, options: [:])
            } else {
                UIApplication.shared.openURL(request.url!)
            }
            
            return false
        }
        return true
    }
    
    // MARK: - Navigation
    
    @IBAction func close(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pushBack(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: AnyObject) {
        pushTo.homeFrom(viewController: self)
    }
}

extension UIColor {
    var hexString:NSString {
        let colorRef = self.cgColor.components
        
        let r:CGFloat = colorRef![0]
        let g:CGFloat = colorRef![1]
        let b:CGFloat = colorRef![2]
        
        return NSString(format: "#%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
    }
}
