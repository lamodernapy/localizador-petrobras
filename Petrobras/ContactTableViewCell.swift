//
//  ContactTableViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/11/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet var titleTextField: UILabel!
    @IBOutlet var textField: UITextField!
    @IBOutlet var iconAction: UIImageView!
    @IBOutlet var titleAction: UILabel!
    @IBOutlet var textView: UITextView!
    @IBOutlet var button: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureTextFieldCellWith(data: [String: String]) {
        titleTextField.text = data["text"]
    }
    
    func configureActionCellWith(data: [String: String]) {
        titleAction.text = data["text"]
        if let iconString = data["icon"] {
            if iconString.isEmpty == false {
                iconAction.image = UIImage(named: iconString)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
