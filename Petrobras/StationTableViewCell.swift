//
//  StationTableViewCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/12/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import Kingfisher

class StationTableViewCell: UITableViewCell {

    @IBOutlet var name: UILabel!
    @IBOutlet var address: UILabel!
    @IBOutlet var wrapperView: UIView!
    @IBOutlet var distance: UILabel!
    @IBOutlet var wrapperIcons: UIView!
    @IBOutlet var product: UILabel!
    
    var services: [Service]!
    var products: [Product]!
    var station: Station! {
        didSet {
            name.text = station.name
            address.text = station.address
            if station.distance != nil{
                distance.text = "\(station.distance!) km"
            } else {
                distance.isHidden = true
            }
            
            let productsName = products.map { $0.name! }
            product.text = productsName.joined(separator: " • ")
        }
    }
    
    override func draw(_ rect: CGRect) {
        var pointer: Int = 0
        for i in 0..<services.count {
            let service = services[i]
            let imageView = UIImageView()
            switch  10.0 + 24.0 * CGFloat(i) <= wrapperIcons.bounds.size.width - 20.0 {
            case true:
                imageView.frame = CGRect(x: 24 * CGFloat(i) + 10, y: 5.0, width: 24, height: 24)
                pointer = i
            case false:
                imageView.frame = CGRect(x: 24 * CGFloat(i-pointer-1) + 10, y: 34.0, width: 24, height: 24)
            }
            
            let sape = station.services?.contains(service.id!)
            
            switch sape! {
            case true:
                if let thumbnailOn = service.thumbnailOn {
                    UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.settingData, completion: { (result, object) in
                        switch result {
                        case true:
                            let rootImgUrl = object as? String
                            let url = URL(string: "\(rootImgUrl!)\(thumbnailOn.url!)")
                            imageView.kf.indicatorType = .activity
                            imageView.kf.setImage(with: url)
                        case false:
                            imageView.backgroundColor = .gray
                        }
                    })
                }
            case false:
                if let thumbnailOff = service.thumbnailOff {
                    UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.settingData, completion: { (result, object) in
                        switch result {
                        case true:
                            let rootImgUrl = object as? String
                            let url = URL(string: "\(rootImgUrl!)\(thumbnailOff.url!)")
                            imageView.kf.indicatorType = .activity
                            imageView.kf.setImage(with: url)
                        case false:
                            imageView.backgroundColor = .gray
                        }
                    })
                }
            }
            wrapperIcons.addSubview(imageView)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        wrapperView.layer.borderWidth = 2
        wrapperView.layer.borderColor = UIColor(red:0.84, green:0.84, blue:0.84, alpha:1.0).cgColor
    }
    
    func setWithStation(data: Station) {
        name.text = data.name
        address.text = data.address
        if data.distance != nil{
            distance.text = "\(data.distance!) km"
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        switch highlighted  {
        case true:
            let overlay = UIView(frame: wrapperView.bounds)
            overlay.tag = 123
            overlay.backgroundColor = CustomColors.blue.withAlphaComponent(0.4)
            wrapperView.addSubview(overlay)
        case false:
            let overlay = super.viewWithTag(123)
            overlay?.removeFromSuperview()
        }
    }
}
