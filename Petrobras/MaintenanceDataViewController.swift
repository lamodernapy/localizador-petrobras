//
//  MaintenanceDataViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/1/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import DatePickerDialog
import EZAlertController

class MaintenanceDataViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, PickerDelegate {
    @IBOutlet var tableView: UITableView!
    
    let fields = [
        ["icon": "",
         "text": "Fecha:"],
        ["icon": "",
         "text": "Kilometraje actual:"],
        ["icon": "",
         "text": "Costo:"],
        ["icon": "icon-chevron",
         "text": "Tipo"]
    ]
    
    var maintenances: [Maintenance] = []
    var maintenanceString: [String] = []
    
    var km: Int = 0
    var amount: Int = 0
    var maintenance: Maintenance!
    var dateSelected: Date!
    
    var cellSelected: Int!
    var maintenanceSelected: Int?
    
    override func viewWillAppear(_ animated: Bool) {
        let data = UserDefaults.standard.object(forKey: UserDefaultsKey.maintenanceListData) as? Data
        maintenances = NSKeyedUnarchiver.unarchiveObject(with: data!) as! [Maintenance]
        maintenanceString = maintenances.map { $0.name }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Costo Mantenimiento"
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height * 0.15)
        tableView.tableFooterView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height * 0.2)
    }
    
    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fields.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 3:
            return 120
        default:
            return 90
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MyCarTableViewCell!
        let field = fields[indexPath.row]
        
        switch indexPath.row {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "date-cell") as! MyCarTableViewCell
            cell.configureWith(data: field)
        case 3:
            cell = tableView.dequeueReusableCell(withIdentifier: "pickerview-cell") as! MyCarTableViewCell
            cell.configureWith(data: field)
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "textview-cell") as! MyCarTableViewCell
            cell.textfield.delegate = self
            cell.textfield.tag = indexPath.row
            cell.configureWith(data: field)
        }
//        if indexPath.row == 3 {
//            cell = tableView.dequeueReusableCell(withIdentifier: "pickerview-cell") as! MyCarTableViewCell
//            cell.configureWith(data: field)
//        } else {
//            cell = tableView.dequeueReusableCell(withIdentifier: "textview-cell") as! MyCarTableViewCell
//            cell.textfield.delegate = self
//            cell.textfield.tag = indexPath.row
//            cell.configureWith(data: field)
//            
////            if indexPath.row == 0 {
////                cell.textfield.addTarget(self, action: #selector(showDatePicker(_:)), for: .touchUpInside)
////            }
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cellSelected = indexPath.row
        
        switch indexPath.row {
        case 0:
            DatePickerDialog().show("Seleccione la fecha", doneButtonTitle: "Aceptar", cancelButtonTitle: "Cancelar", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date, callback: { (date) in
                print("\(date)")
                if let dateSelected = date {
                    let cell = tableView.cellForRow(at: indexPath) as! MyCarTableViewCell
                    cell.selectedDate.text = FormatDate.event(date: dateSelected)
                    self.dateSelected = dateSelected
                }
            })
        case 3:
            self.performSegue(withIdentifier: SegueIdentifier.pushToPicker, sender: maintenanceString)
        default:
            let cell = tableView.cellForRow(at: indexPath) as! MyCarTableViewCell
            cell.textfield.becomeFirstResponder()
        }
        
//        if indexPath.row == 3 {
//            
//        } else if indexPath.row == 0 {
//            DatePickerDialog().show(title: "DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
//                (date) -> Void in
//                print("\(date)")
//            }
//        } else {
//            let cell = tableView.cellForRow(at: indexPath) as! MyCarTableViewCell
//            cell.textfield.becomeFirstResponder()
//        }
    }
    
    // MARK: TextField
    
    @IBAction func textFieldEdit(_ sender: UITextField) {
        if let text = sender.text {
            cellSelected = sender.tag
            switch cellSelected {
            case 1:
                km = text.isEmpty == false ? Int(text)! : 0
            case 2:
                amount = text.isEmpty == false ? Int(text)! : 0
            default:
                break
            }
        }
    }
    
    // MARK: PickerDelegate
    
    func userSelect(index: Int) {
        let cell = tableView.cellForRow(at: IndexPath(row: cellSelected, section: 0)) as! MyCarTableViewCell
        if cellSelected == 3 {
            if index != maintenanceSelected {
                maintenanceSelected = index
                maintenance = maintenances[index]
                cell.selectedOption.text = maintenance.name
            }
        }
    }
    
    @IBAction func saveMaintenanceData(_ sender: AnyObject) {
        if km != 0 && amount != 0 && maintenance != nil && dateSelected != nil {
            let maintenanceDic: [String: Any] = ["km": km,
                                                 "amount": amount,
                                                 "maintenance" : maintenance,
                                                 "date": dateSelected]
            let maintenanceData = MaintenanceData(dictionary: maintenanceDic)
            var maintenanceDataArray: [MaintenanceData] = []
            
            UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.maintenanceData, completion: { (success, data) in
                switch success {
                case true:
                    guard let maintenanceHelper = data as? [MaintenanceData] else {
                        EZAlertController.alert("Error", message: "No se pudieron guardar los datos. Favor reintentar.")
                        return
                    }
                    
                    maintenanceDataArray = maintenanceHelper
                    
                    if let lastKm = maintenanceDataArray.last?.km {
                        if lastKm < self.km {
                            maintenanceDataArray.append(maintenanceData)
                            maintenanceDataArray.sort(by: { $0.date < $1.date })
                            UserDefaultsClasses.save(object: maintenanceDataArray, withKey: UserDefaultsKey.maintenanceData)
                            
                            let allViewController = self.navigationController?.viewControllers
                            for viewController in allViewController! {
                                if viewController.isKind(of: MyCarProfileViewController.self) {
                                    _ = viewController.navigationController?.popToViewController(viewController, animated: true)
                                }
                            }
                        } else {
                            let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! MyCarTableViewCell
                            cell.message.text = "El kilometraje debe ser mayor al mantenimiento anterior."
                        }
                    }
                case false:
                    maintenanceDataArray.append(maintenanceData)
                    maintenanceDataArray.sort(by: { $0.date < $1.date })
                    UserDefaultsClasses.save(object: maintenanceDataArray, withKey: UserDefaultsKey.maintenanceData)
                    
                    let allViewController = self.navigationController?.viewControllers
                    for viewController in allViewController! {
                        if viewController.isKind(of: MyCarProfileViewController.self) {
                            _ = viewController.navigationController?.popToViewController(viewController, animated: true)
                        }
                    }
                }
                
            })
            
            
//            let data = UserDefaults.standard.object(forKey: UserDefaultsKey.maintenanceData) as? Data
//            if data == nil {
//                maintenanceDataArray.append(maintenanceData)
//                let saveMaintenanceData  = NSKeyedArchiver.archivedData(withRootObject: maintenanceDataArray)
//                UserDefaults.standard.set(saveMaintenanceData, forKey: UserDefaultsKey.maintenanceData)
//                let allViewController = self.navigationController?.viewControllers
//                for viewController in allViewController! {
//                    if viewController.isKind(of: MyCarProfileViewController.self) {
//                        _ = viewController.navigationController?.popToViewController(viewController, animated: true)
//                    }
//                }
//            } else {
//                maintenanceDataArray = NSKeyedUnarchiver.unarchiveObject(with: data!) as! [MaintenanceData]
//                if let lastKm = maintenanceDataArray.last?.km {
//                    if lastKm < km {
//                        maintenanceDataArray.append(maintenanceData)
//                        let saveMaintenanceData  = NSKeyedArchiver.archivedData(withRootObject: maintenanceDataArray)
//                        UserDefaults.standard.set(saveMaintenanceData, forKey: UserDefaultsKey.maintenanceData)
//                        let allViewController = self.navigationController?.viewControllers
//                        for viewController in allViewController! {
//                            if viewController.isKind(of: MyCarProfileViewController.self) {
//                                _ = viewController.navigationController?.popToViewController(viewController, animated: true)
//                            }
//                        }
//                    } else {
//                        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! MyCarTableViewCell
//                        cell.message.text = "El kilometraje debe ser mayor al mantenimiento anterior."
//                    }
//                }
//            }
        } else {
            for i in 0..<4 {
                let cell = tableView.cellForRow(at: IndexPath(row: i, section: 0)) as! MyCarTableViewCell
                switch i {
                case 0:
                    cell.message.text = dateSelected == nil ? "Seleccione una fecha." : ""
                case 1:
                    cell.message.text = km == 0 ? "Ingrese su kilometraje actual." : ""
                case 2:
                    cell.message.text = amount == 0 ? "Ingrese el costo del mantenimiento." : ""
                case 3:
                    cell.message.text = maintenance == nil ? "Elija el tipo de mantenimiento." : ""
                default:
                    break
                }
            }
        }
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.pushToPicker {
            let pickerVC: PickerViewController = segue.destination as! PickerViewController
            pickerVC.options = sender as! [String]
            pickerVC.delegate = self
            switch cellSelected {
            case 3:
                pickerVC.selectedOptions = maintenanceSelected
                pickerVC.titleForNavigationBar = "Tipo de mantenimiento"
            default:
                break
            }
        }
    }

    @IBAction func pushBack(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: AnyObject) {
        pushTo.homeFrom(viewController: self)
    }

}
