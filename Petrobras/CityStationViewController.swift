//
//  CityStationViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/13/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import EZAlertController

class CityStationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PickerDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    let fields = [
        ["icon": "icon-chevron",
         "text": "Ciudad"]
    ]
    
    var cities: [City] = []
    var citiesString: [String] = []
    var citySelected: Int?
    var city: City!
    var stations: [Station] = []
    
    override func viewWillAppear(_ animated: Bool) {
        if cities.count == 0 {
            let regionData = UserDefaults.standard.object(forKey: UserDefaultsKey.regionData) as! Data
            let regions = NSKeyedUnarchiver.unarchiveObject(with: regionData) as! [Region]
            
            for region in regions {
                for city in region.cities {
                    cities.append(city)
                }
            }
            
            cities = cities.sorted { $0.name! < $1.name! }
            citiesString = cities.map { $0.name! }
            
            let stationsData = UserDefaults.standard.object(forKey: UserDefaultsKey.stationsData) as! Data
            stations = NSKeyedUnarchiver.unarchiveObject(with: stationsData) as! [Station]
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
		
		setNavbarDivider()

        self.title = "Búsqueda"
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight * 0.15)
        tableView.tableFooterView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight * 0.2)
    }

    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fields.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let field = fields[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "pickerview-cell") as! MyCarTableViewCell
        cell.configureWith(data: field)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: SegueIdentifier.pushToPicker, sender: citiesString)
        }
    }
    
    // MARK: PickerDelegate
    
    func userSelect(index: Int) {
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! MyCarTableViewCell
        if index != citySelected {
            citySelected = index
            city = cities[index]
            cell.selectedOption.text = city.name
        }
    }
    
    // MARK: - Navigation
    
    @IBAction func pushBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.pushToPicker {
            let pickerVC: PickerViewController = segue.destination as! PickerViewController
            pickerVC.options = sender as! [String]
            pickerVC.delegate = self
            pickerVC.selectedOptions = citySelected
            pickerVC.titleForNavigationBar = "Elija su ciudad"
        } else if segue.identifier == SegueIdentifier.pushToStationList {
            let vc: StationListViewController = segue.destination as! StationListViewController
            vc.stations = sender as! [Station]
        }
    }

    @IBAction func stationsFilteredByCity(_ sender: Any) {
        if city != nil {
            let stations = self.stations.filter { $0.city == city.id }
            if stations.count == 0 {
                EZAlertController.alert("No tiene estaciones disponibles!")
            } else {
                performSegue(withIdentifier: SegueIdentifier.pushToStationList, sender: stations)
            }
        } else {
            EZAlertController.alert("Error", message: "Elija alguna ciudad")
        }
    }
    
    
    @IBAction func pushToHome(_ sender: AnyObject) {
        pushTo.homeFrom(viewController: self)
    }
    
}
