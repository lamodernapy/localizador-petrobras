//
//  Promo.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/16/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation

class Promo: NSObject, NSCoding {
    var id: String!
    var title: String!
    var descriptionPromo: String!
    var image: File?
    var startDate: Date!
    var finishDate: Date!
    var days: Int!
    var imageUrl :String?
    
    init (dictionary: [String: Any]) {
        id = dictionary["id"] as? String
        title  = dictionary["title"] as? String
        descriptionPromo = dictionary["description"] as? String
        days = dictionary["days"] as? Int
		
        if let time = dictionary["timerange"] as? [String: Any] {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
			dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
			dateFormatter.locale = Locale(identifier: "en_US_POSIX")
			
            startDate = dateFormatter.date(from: time["starts"] as! String)
            finishDate = dateFormatter.date(from: time["ends"] as! String)
        }
		
        if let imageId = dictionary["image"] as? String {
            let filesData = UserDefaults.standard.object(forKey: UserDefaultsKey.filesData) as! Data
            let files = NSKeyedUnarchiver.unarchiveObject(with: filesData) as! [File]
            for file in files {
                if file.id == imageId {
                    image = file
                }
            }
        }
    }
    
    init (notificationDictionary: [String: Any]) {
        id = notificationDictionary["id"] as? String
        title  = notificationDictionary["title"] as? String
        descriptionPromo = notificationDictionary["description"] as? String
        days = notificationDictionary["days"] as? Int
        if let time = notificationDictionary["timerange"] as? [String: Any] {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            startDate = dateFormatter.date(from: time["starts"] as! String)
            finishDate = dateFormatter.date(from: time["ends"] as! String)
        }
        
        let imageData = notificationDictionary["image"] as! [String: Any]
        imageUrl = imageData["url"] as? String
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? String
        title  = aDecoder.decodeObject(forKey: "title") as? String
        descriptionPromo = aDecoder.decodeObject(forKey: "descriptionPromo") as? String
        image = aDecoder.decodeObject(forKey: "image") as? File
        startDate = aDecoder.decodeObject(forKey: "startDate") as? Date
        finishDate = aDecoder.decodeObject(forKey: "finishDate") as? Date
        days = aDecoder.decodeObject(forKey: "days") as? Int
        imageUrl = aDecoder.decodeObject(forKey: "imageUrl") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(descriptionPromo, forKey: "descriptionPromo")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(startDate, forKey: "startDate")
        aCoder.encode(finishDate, forKey: "finishDate")
        aCoder.encode(days, forKey: "days")
        aCoder.encode(imageUrl, forKey: "imageUrl")
    }
}
