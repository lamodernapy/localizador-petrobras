//
//  Event.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 7/5/18.
//  Copyright © 2018 Agencia Lamoderna. All rights reserved.
//

import Foundation

import Alamofire

class Event: NSObject, NSCoding {
    var id: String!
    var title: String!
    var desc: String!
    var kmz: File!
    var date: String!
    var hashtags: [String]?
    
    init (dictionary: [String: Any]) {
        id = dictionary["id"] as? String
        title  = dictionary["title"] as? String
        desc = dictionary["description"] as? String
//        kmz = dictionary["kmz"] as? File
        if let stringDate = dictionary["date"] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: stringDate)
            
            let longDate = DateFormatter()
            longDate.dateStyle = .long
            longDate.timeStyle = .short
            
            self.date = longDate.string(from: date!)
        }
        
        hashtags = dictionary["hashtags"] as? [String]
        
        if  let kmzId = dictionary["kmz"] as? String {
            let filesData = UserDefaults.standard.object(forKey: UserDefaultsKey.filesData) as! Data
            let files = NSKeyedUnarchiver.unarchiveObject(with: filesData) as! [File]
            for file in files {
                if file.id == kmzId {
                   kmz = file
                }
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? String
        title  = aDecoder.decodeObject(forKey: "title") as? String
        desc = aDecoder.decodeObject(forKey: "desc") as? String
        kmz = aDecoder.decodeObject(forKey: "kmz") as? File
        date = aDecoder.decodeObject(forKey: "date") as? String
        hashtags = aDecoder.decodeObject(forKey: "hashtags") as? [String]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(desc, forKey: "desc")
        aCoder.encode(kmz, forKey: "kmz")
        aCoder.encode(date, forKey: "date")
        aCoder.encode(hashtags, forKey: "hashtags")
    }
}
