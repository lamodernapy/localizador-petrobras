//
//  Service.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 12/12/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation

class Service: NSObject, NSCoding {
    var id: String?
    var name: String?
    var priority: Int?
    var image: File?
    var thumbnailOn: File?
    var thumbnailOff: File?
    
    init (dictionary: [String: Any]) {
        id = dictionary["id"] as? String
        name  = dictionary["name"] as? String
        priority = dictionary["priority"] as? Int
        
        if let imageHelper = dictionary["images"] as? [String: String] {
            if let mainId = imageHelper["main"],
                let thumbnailOnId = imageHelper["on"],
                let thumbnailOffId = imageHelper["off"] {
                let filesData = UserDefaults.standard.object(forKey: UserDefaultsKey.filesData) as! Data
                let files = NSKeyedUnarchiver.unarchiveObject(with: filesData) as! [File]
                for file in files {
                    if file.id == mainId {
                        image = file
                    } else if file.id == thumbnailOnId {
                        thumbnailOn = file
                    } else if file.id == thumbnailOffId {
                        thumbnailOff = file
                    }
                }
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? String
        name  = aDecoder.decodeObject(forKey: "name") as? String
        priority = aDecoder.decodeObject(forKey: "priority") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? File
        thumbnailOn = aDecoder.decodeObject(forKey: "thumbnailOn") as? File
        thumbnailOff = aDecoder.decodeObject(forKey: "thumbnailOff") as? File
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(priority, forKey: "priority")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(thumbnailOn, forKey: "thumbnailOn")
        aCoder.encode(thumbnailOff, forKey: "thumbnailOff")
    }
}
