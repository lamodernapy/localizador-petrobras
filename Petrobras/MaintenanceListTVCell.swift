//
//  MaintenanceListTVCell.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 2/13/17.
//  Copyright © 2017 Agencia Lamoderna. All rights reserved.
//

import UIKit

class MaintenanceListTVCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var km: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var cost: UILabel!
    @IBOutlet var optionButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
