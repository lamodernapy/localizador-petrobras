//
//  Helper.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 11/29/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import Foundation
import UIKit
import LinearProgressBarMaterial

struct CustomColors {
    static let textColor = UIColor(red:0.00, green:0.27, blue:0.42, alpha:1.0)
    static let green = UIColor(red:0.00, green:0.53, blue:0.25, alpha:1.0)
    static let yellow = UIColor(red:1.00, green:0.82, blue:0.00, alpha:1.0)
    static let blue = UIColor(red:0.00, green:0.38, blue:0.61, alpha:1.0)
}

struct SegueIdentifier {
    static let pushToHome = "pushToHomeViewController"
    static let pushToCreateMyCar = "pushToCreateMyCarViewController"
    static let pushToMyCarProfile = "pushToMyCarProfileViewController"
    static let pushToPicker = "pushToMyPickerViewController"
    static let pushToEditMyCar = "editCar"
    static let pushToContact = "pushToContactViewController"
    static let pushToStationList = "pushToStationList"
    static let pustToCityStation = "pushToCityStation"
    static let pushToCustomSearch = "pushToCustomSearch"
    static let pushToGasCylinder = "pushToGasCylinder"
    static let pushToPromo = "pushToPromo"
    static let pushToPromoDetail = "pushToPromoDetail"
    static let pusthToGMaps = "showGMaps"
    static let pushToMaintenanceList = "pushToMaintenanceList"
    static let pushToEventList = "pushToEventListViewController"
    static let pushToEventDetail = "pushToEventDetailViewController"
    static let pushToRallyMap = "pushToRallyMapViewController"
}

struct CellIdentifier {
    static let picker = "picker-cell"
}

struct UserDefaultsKey {
    static let carData = "car-data"
    static let brandsData = "brands-models-data"
    static let avatar = "avatar"
    static let stationsData = "stations-data"
    static let productsData = "products-data"
    static let fuelData = "fuel-data"
    static let maintenanceListData = "maintenance-list-data"
    static let maintenanceData = "maintenance-data"
    static let serviceData = "service-data"
    static let filesData = "files-data"
    static let regionData = "region-data"
    static let nearestStationData = "nearest-station-data"
    static let gasData = "gas-data"
    static let promoData = "promo-data"
    static let settingData = "settings-data"
    static let userData = "user-data"
    static let cookies = "cookies"
    static let eventsData = "events-data"
}

struct RequestUrl {
//    static let masterJson = "http://petrobras.dev.agenciamoderna.com.py/webservice/html/json/develop.json"
//    static let masterJson = "http://petrobras.dev.agenciamoderna.com.py/json/develop.json" //dev
    static let masterJson = "http://petrobras.deploy.agenciamoderna.com.py/json/master.json" //produccion
    
//    static let root = "http://petrobras.dev.agenciamoderna.com.py/webservice/html/api/v1.0"
//    static let root = "http://petrobras.dev.agenciamoderna.com.py/" //dev
    static let root = "http://petrobras.deploy.agenciamoderna.com.py/" //produccion
    static let login = "/login"
    static let register = "/register"
    static let contact = "/contact"
    static let promo = "/promotions"
}

class FormatDate {
    
    static func event(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateHelper = dateFormatter.string(from: date)
        return dateHelper
    }
    
    static func stringTo(date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        
        let dateObj = dateFormatter.date(from: date)
        return dateObj!
    }
}

class UserDefaultsClasses {
    static func save(object: Any, withKey: String) {
        let data = NSKeyedArchiver.archivedData(withRootObject: object)
        print(data)
        UserDefaults.standard.set(data, forKey: withKey)
    }
    
    static func checkIfExists(key: String, completion:@escaping (_ result: Bool, _ object: Any?) -> Void) {
        let data = UserDefaults.standard.object(forKey: key) as? Data
        if data == nil {
            completion(false, nil)
        } else {
            let object = NSKeyedUnarchiver.unarchiveObject(with: data!)
            completion(true, object)
        }
    }
    
    static func removeObjectWith(key: String) {
        UserDefaults.standard.removeObject(forKey: key)
    }
}

class pushTo {
    static func homeFrom(viewController: UIViewController) {
        let allViewController = viewController.navigationController?.viewControllers
        for viewController in allViewController! {
            if viewController.isKind(of: HomeViewController.self) {
                _ = viewController.navigationController?.popToViewController(viewController, animated: true)
            } else {
                viewController.dismiss(animated: true, completion: nil)
            }
        }
    }
}

class Loader {
    static let linearBar: LinearProgressBar = LinearProgressBar()
    
    static func start() {
        linearBar.backgroundColor = CustomColors.green
        linearBar.progressBarColor = CustomColors.yellow

        linearBar.heightForLinearBar = 5
        linearBar.startAnimation()
    }
    
    static func stop() {
        linearBar.stopAnimation()
    }
}

class ScreenSize {
    static let main = UIScreen.main.bounds
    static let width = main.width
    static let height = main.height
}

// MARK: - Format number

func format(number: Int) -> String {
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = NumberFormatter.Style.decimal
    return numberFormatter.string(from: NSNumber(value: number))!
}

