//
//  CreateMyCarViewController.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 11/29/16.
//  Copyright © 2016 Agencia Lamoderna. All rights reserved.
//

import UIKit
import EZAlertController

class CreateMyCarViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, PickerDelegate {

    @IBOutlet var tableView: UITableView!
    
    let fields = [
        ["icon": "icon-chevron",
         "text": "Marca"],
        ["icon": "",
         "text": "Año"],
        ["icon": "icon-chevron",
         "text": "Automóvil"]
    ]
    
    var brands: [Brand]?
    var models: [Model]?
    var brandsString: [String]?
    var modelsString: [String]?
    
    var brand: Brand!
    var model: Model!
    var year: String!
    var avatar: UIImage!
    
    var cellSelected: Int!
    var brandSelected : Int?
    var modelSelected: Int?
    
    var edit: Bool = false
    var questionPreSave: Bool = false
    var pushBack: Bool = false
    var myCar: Car?
    var myCarAvatar: UIImage?
    
    override func viewWillAppear(_ animated: Bool) {
        UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.brandsData, completion: { (result, object) in
            switch result {
            case true:
                self.brands = object as? [Brand]
                if let brands = self.brands {
                    self.brandsString = brands.map { $0.name }
                }
            case false:
                break
            }
        })
        
        if edit == true {
            UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.carData, completion: { (result, object) in
                switch result {
                case true:
                    self.myCar = object as? Car
                case false:
                    self.myCar = nil
                }
            })
            
            UserDefaultsClasses.checkIfExists(key: UserDefaultsKey.avatar, completion: { (result, object) in
                switch result {
                case true:
                    self.myCarAvatar = object as? UIImage
                case false:
                    self.myCarAvatar = nil
                }
            })
            
            guard let brands = self.brands,
            let myCar = self.myCar
            else {
                EZAlertController.alert("No disponemos de algunos datos.", message: "", acceptMessage: "Aceptar", acceptBlock: {
                    if let nav = self.navigationController {
                        nav.popViewController(animated: true)
                    }
                })
                return
            }
            
            brandSelected = brands.index(where: { $0.id == myCar.brand?.id })
            brand = brands[brandSelected!]
            models = brand.models
            
            guard let models = self.models
            else {
                EZAlertController.alert("No disponemos de algunos datos.", message: "", acceptMessage: "Aceptar", acceptBlock: {
                    if let nav = self.navigationController {
                        nav.popViewController(animated: true)
                    }
                })
                return
            }
            
            modelsString = models.map { $0.name }
            modelSelected = models.index(where: { $0.id == myCar.model?.id })
            model = models[modelSelected!]
            year = myCar.year
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Mi auto"
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: CustomColors.blue,
            NSAttributedStringKey.font: UIFont(name: "TrebuchetMS-Bold", size: 18)!
        ]
        
        tableView.tableHeaderView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height * 0.15)
        tableView.tableFooterView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height * 0.2)
    }

    // MARK: - UITableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fields.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MyCarTableViewCell!
        let field = fields[indexPath.row]
        if indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 3 {
            cell = tableView.dequeueReusableCell(withIdentifier: "pickerview-cell") as! MyCarTableViewCell
            cell.configureWith(data: field)
            if edit == true {
                switch indexPath.row {
                case 0:
                    cell.selectedOption.text = myCar != nil ? myCar?.brand?.name : "--"
                case 2:
                    cell.selectedOption.text = myCar != nil ? myCar?.model?.name : "--"
                default:
                    break
                }
            }
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "textview-cell") as! MyCarTableViewCell
            cell.textfield.delegate = self
            cell.textfield.tag = indexPath.row
            cell.configureWith(data: field)
            if edit == true {
                cell.textfield.text = myCar != nil ? myCar?.year : "--"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        edit = false
        cellSelected = indexPath.row
        
        switch indexPath.row {
        case 0:
            self.performSegue(withIdentifier: SegueIdentifier.pushToPicker, sender: brandsString)
        case 1:
            let cell = tableView.cellForRow(at: indexPath) as! MyCarTableViewCell
            cell.textfield.becomeFirstResponder()
        case 2:
            if brandSelected == nil {
                EZAlertController.alert("Alerta!", message: "Elija primero una marca.")
            } else {
                guard let models = self.models else {
                    EZAlertController.alert("No disponemos de algunos datos.", message: "", acceptMessage: "Aceptar", acceptBlock: {
                        if let nav = self.navigationController {
                            nav.popViewController(animated: true)
                        }
                    })
                    return
                }
                
                let modelsName = models.map { $0.name }
                self.performSegue(withIdentifier: SegueIdentifier.pushToPicker, sender: modelsName)
            }
        default:
            break
        }
    }
    
    // MARK: - UITextField
    
    @IBAction func textFieldEdit(_ sender: UITextField) {
        cellSelected = sender.tag
        year = sender.text!
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 4
    }
    
    // MARK: PickerDelegate
    
    func userSelect(index: Int) {
        let cell = tableView.cellForRow(at: IndexPath(row: cellSelected, section: 0)) as! MyCarTableViewCell
        
        switch cellSelected {
        case 0:
            if index != brandSelected {
                brandSelected = index
                
                guard let brands = self.brands else {
                    EZAlertController.alert("No disponemos de algunos datos.", message: "", acceptMessage: "Aceptar", acceptBlock: {
                        if let nav = self.navigationController {
                            nav.popViewController(animated: true)
                        }
                    })
                    return
                }
                
                brand = brands[index]
                cell.selectedOption.text = brand.name
                models = brand.models
                
                guard let models = self.models else {
                    EZAlertController.alert("No disponemos de algunos datos.", message: "", acceptMessage: "Aceptar", acceptBlock: {
                        if let nav = self.navigationController {
                            nav.popViewController(animated: true)
                        }
                    })
                    return
                }
                
                modelsString = models.map { $0.name }
                let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? MyCarTableViewCell
                cell?.selectedOption.text = ""
            }
        case 2:
            if index != modelSelected {
                modelSelected = index
                guard let models = self.models else {
                    EZAlertController.alert("No disponemos de algunos datos.", message: "", acceptMessage: "Aceptar", acceptBlock: {
                        if let nav = self.navigationController {
                            nav.popViewController(animated: true)
                        }
                    })
                    return
                }
                
                model = models[index]
                cell.selectedOption.text = model.name
            }
        default:
            break
        }
    }
    
    // MARK: Save Button
    
    @IBAction func saveMyCar(_ sender: AnyObject) {
        if brand != nil && year != nil && model != nil {
            if questionPreSave == true {
                guard let myCar = self.myCar else {
                    return
                }
                
                if brand != myCar.brand || model != myCar.model || year != myCar.year  {
                    EZAlertController.alert("Alerta", message: "Si cambia de vehículo se perderan todos los datos del anterior vehículo", buttons: ["Aceptar", "Cancelar"]) { (alertAction, position) -> Void in
                        if position == 0 {
                            UserDefaultsClasses.removeObjectWith(key: UserDefaultsKey.carData)
                            UserDefaultsClasses.removeObjectWith(key: UserDefaultsKey.fuelData)
                        }
                    }
                }
            }
            
            let carDic: [String: Any] = ["brand": brand,
                                         "year": year,
                                         "model": model]
            
            let car = Car(dictionary: carDic)
            UserDefaultsClasses.save(object: car, withKey: UserDefaultsKey.carData)
            
            if avatar != nil {
                UserDefaultsClasses.save(object: avatar, withKey: UserDefaultsKey.avatar)
            }
            
            if pushBack == true {
                if let nav = self.navigationController {
                    nav.popViewController(animated: true)
                }
            } else {
                self.performSegue(withIdentifier: SegueIdentifier.pushToMyCarProfile, sender: nil)
            }
        } else {
            for i in 0..<3 {
                let cell = tableView.cellForRow(at: IndexPath(row: i, section: 0)) as! MyCarTableViewCell
                switch i {
                case 0:
                    cell.message.text = brand == nil ? "Debe completar la marca." : ""
                case 1:
                    cell.message.text = year == nil ? "Debe completar el año." : ""
                case 2:
                    cell.message.text = model == nil ? "Debe completar el modelo." : ""
                default:
                    break
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.pushToPicker {
            let pickerVC: PickerViewController = segue.destination as! PickerViewController
            pickerVC.options = sender as! [String]
            pickerVC.delegate = self
            switch cellSelected {
            case 0:
                pickerVC.selectedOptions = brandSelected
                pickerVC.titleForNavigationBar = "Elija su marca"
            case 2:
                pickerVC.selectedOptions = modelSelected
                pickerVC.titleForNavigationBar = "Elija su modelo"
            default:
                break
            }
        }
    }
    
    @IBAction func pushBack(_ sender: AnyObject) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    @IBAction func pushToHome(_ sender: AnyObject) {
        pushTo.homeFrom(viewController: self)
    }
}
