//
//  HeaderMaintenance.swift
//  Petrobras
//
//  Created by Gustavo Leguizamon on 2/13/17.
//  Copyright © 2017 Agencia Lamoderna. All rights reserved.
//

import UIKit

class HeaderMaintenance: UITableViewCell {

    @IBOutlet var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
